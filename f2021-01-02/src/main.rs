use chrono::Local;
use nannou::prelude::*;
use nannou::rand::rngs::ThreadRng;
use nannou::rand::seq::SliceRandom;
use nannou::rand::thread_rng;

use std::collections::{BTreeMap, HashSet};
use std::default::Default;

const NUM_DOTS: usize = 3500;
const MIN_RAD: f32 = 2.0;
const MAX_RAD: f32 = 5.0;
const MIN_EX: f32 = 1.0;
const MAX_EX: f32 = 4.0;
const MAX_ITERATIONS: u32 = 20;

#[derive(Debug, Default)]
struct Dot {
    v_rad: f32,
    ex_rad: f32,
    pos: Vector2,
    hue: f32,
    value: f32,
}

#[derive(Debug)]
struct Model {
    dots: Vec<Dot>,
    active: HashSet<usize>,
    by_x: BTreeMap<i32, Vec<usize>>,
    by_y: BTreeMap<i32, Vec<usize>>,
    rng: ThreadRng,
    rect: Rect,
    now: String,
    caps: usize,
}

impl Default for Model {
    fn default() -> Self {
        Model {
            dots: Vec::new(),
            active: HashSet::new(),
            by_x: BTreeMap::new(),
            by_y: BTreeMap::new(),
            rng: thread_rng(),
            rect: Rect::from_w_h(1400.0, 800.0),
            now: format!("{}", Local::now().format("%H:%M:%S")),
            caps: 0,
        }
    }
}

impl Model {
    pub fn new() -> Self {
        Model {
            dots: Vec::with_capacity(NUM_DOTS),
            ..Default::default()
        }
    }
    pub fn now(&self) -> &str {
        &self.now
    }

    pub fn neighbors(&self, candidate: &Dot) -> Vec<usize> {
        let mut x_set: HashSet<usize> = HashSet::new();
        let mut y_set: HashSet<usize> = HashSet::new();
        let c_x = candidate.pos.x;
        let c_y = candidate.pos.y;
        let radius = candidate.ex_rad;
        let min_x = (c_x - (1.1 * radius)).floor() as i32;
        let max_x = (c_x + (1.1 * radius)).ceil() as i32;
        let min_y = (c_y - (1.1 * radius)).floor() as i32;
        let max_y = (c_y + (1.1 * radius)).ceil() as i32;

        for (_, v) in self.by_x.range(min_x..=max_x) {
            for i in v.iter() {
                x_set.insert(*i);
            }
        }
        for (_, v) in self.by_y.range(min_y..=max_y) {
            for i in v.iter() {
                y_set.insert(*i);
            }
        }

        x_set
            .intersection(&y_set)
            .map(|e| e.to_usize().unwrap())
            .collect()
    }

    pub fn add_from_parent(&mut self, parent: usize) -> bool {
        let parent = &self.dots[parent];
        let mut child = Dot::default();
        child.hue = parent.hue;
        let v_rad = parent.v_rad;
        let ex_rad = v_rad + random_range(MIN_EX, MAX_EX);
        child.ex_rad = ex_rad;
        child.v_rad = v_rad;
        let mut count = 0;
        while count <= MAX_ITERATIONS {
            child.pos = find_pos(&parent.pos, v_rad, ex_rad);
            let neighbors = self.neighbors(&child);
            for i in neighbors.choose_multiple(&mut self.rng, neighbors.len()) {
                let n = &self.dots[*i];
                let d = n.pos - child.pos;
                let d = d.magnitude();
                if d < child.ex_rad || d < n.ex_rad {
                    count += 1;
                } else {
                    break;
                }
            }
        }
        if count < MAX_ITERATIONS {
            let v = ((MAX_ITERATIONS - count) as f32) / (MAX_ITERATIONS as f32);
            child.value = v;
            let idx = self.dots.len();
            let x = child.pos.x as i32;
            let y = child.pos.y as i32;
            self.by_x.entry(x).or_insert(Vec::new()).push(idx);
            self.by_y.entry(y).or_insert(Vec::new()).push(idx);
            self.active.insert(idx);
            self.dots.push(child);
            true
        } else {
            false
        }
    }

    pub fn add(&mut self) {
        let hue = random_f32();
        let v_rad = random_range(MIN_RAD, MAX_RAD);
        let ex_rad = random_range(MIN_EX, MAX_EX) + v_rad;
        let x_range = self.rect.x;
        let y_range = self.rect.y;
        let mut dot = Dot::default();
        dot.v_rad = v_rad;
        dot.ex_rad = ex_rad;
        dot.hue = hue;
        dot.value = 1.0;
        for _ in 0..500 {
            let x = random_range(x_range.start + ex_rad, x_range.end - ex_rad);
            let y = random_range(y_range.start + ex_rad, y_range.end - ex_rad);
            let pos = Vector2::new(x, y);
            dot.pos = pos.clone();
            let neighbors = self.neighbors(&dot);
            if neighbors.is_empty() {
                let idx = self.dots.len();
                let x = x as i32;
                let y = y as i32;
                self.by_x.entry(x).or_insert(Vec::new()).push(idx);
                self.by_y.entry(y).or_insert(Vec::new()).push(idx);
                self.dots.push(dot);
                self.active.insert(idx);
                return;
            }
            for i in neighbors.choose_multiple(&mut self.rng, neighbors.len()) {
                let n = &self.dots[*i];
                let d = n.pos - pos;
                let d = d.magnitude();
                if d < ex_rad || d < n.ex_rad {
                    continue;
                } else {
                    let idx = self.dots.len();
                    let x = x as i32;
                    let y = y as i32;
                    self.by_x.entry(x).or_insert(Vec::new()).push(idx);
                    self.by_y.entry(y).or_insert(Vec::new()).push(idx);
                    self.dots.push(dot);
                    self.active.insert(idx);
                    return;
                }
            }
        }
    }
}

fn find_pos(p: &Vector2, r1: f32, r2: f32) -> Vector2 {
    let rads = random_range(0.0, TAU);
    let radius = random_range(r1, r2);
    let x = (rads.cos() * radius) + p.x;
    let y = (rads.sin() * radius) + p.y;
    Vector2::new(x, y)
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    app.new_window()
        .size(1400, 800)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    Model::new()
}

fn update(app: &App, model: &mut Model, update: Update) {
    let len = model.dots.len();
    if len < NUM_DOTS {
        let mut dels = HashSet::new();
        model.add();
        for i in model.active.clone().iter() {
            if !model.add_from_parent(*i) {
                dels.insert(*i);
            }
        }
        let new_active = model.active.difference(&dels).collect::<HashSet<_>>();
        model.active = new_active.iter().map(|e| e.to_usize().unwrap()).collect();
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    if frame.nth() == 0 || app.keys.down.contains(&Key::B) {
        draw.background().color(BLACK);
    }

    for d in model.dots.iter() {
        let hue = d.hue;
        let value = d.value;
        let radius = d.v_rad;
        let pos = d.pos;
        draw.ellipse()
            .x_y(pos.x, pos.y)
            .radius(radius)
            .hsv(hue, 1.0, value);
    }

    draw.to_frame(app, &frame).unwrap();
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-"
                + &model.caps.to_string()
                + ".png";
            app.main_window().capture_frame(out);
            model.caps += 1;
        }
        _ => {}
    }
}
