use chrono::Local;
use nannou::prelude::*;
use nannou::rand::rngs::StdRng;
use nannou::rand::{Rng, SeedableRng};

use std::cell::RefCell;
use std::default::Default;
use std::thread::sleep_ms;

const RNG_SEED: u64 = 18;
const W_H: f32 = 900.0;

#[derive(Debug)]
struct Model {
    rng: RefCell<StdRng>,
    rect: Rect,
    now: String,
    caps: usize,
}

impl Default for Model {
    fn default() -> Self {
        Model {
            rng: RefCell::new(StdRng::seed_from_u64(RNG_SEED)),
            rect: Rect::from_w_h(W_H, W_H),
            now: format!("{}", Local::now().format("%Y_%m_%d_%H_%M_%S")),
            caps: 0,
        }
    }
}

impl Model {
    pub fn new() -> Self {
        Model {
            ..Default::default()
        }
    }
    pub fn now(&self) -> &str {
        &self.now
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    app.new_window()
        .size(W_H as u32, W_H as u32)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    Model::new()
}

fn update(app: &App, model: &mut Model, update: Update) {}

fn view(app: &App, model: &Model, frame: Frame) {
    app.set_loop_mode(LoopMode::rate_fps(0.01));
    let draw = app.draw();

    if frame.nth() == 0 || app.keys.down.contains(&Key::B) {
        draw.background().hsv(0.088, 0.9, 0.0);
    }

    let mut prev_rad = 200;
    let mut alpha = 0.1;
    let mut hue = 0.088;
    for base_rad in (20..301).step_by(30).rev() {
        dbg!(base_rad);
        let mut pts: Vec<Vector2> = Vec::new();
        let mut degs = 0;
        while degs < 360 {
            dbg!(degs);
            let radian = deg_to_rad(degs as f32);
            let fudge = prev_rad as f32 / 2.0;
            let smear = model
                .rng
                .borrow_mut()
                .gen_range(-fudge - 0.01, fudge + 0.01);
            let radius = base_rad as f32 + smear;
            let x = radian.cos() * radius;
            let y = radian.sin() * radius;
            pts.push(Vector2::new(x, y));
            degs += model.rng.borrow_mut().gen_range(5, 30);
            degs = degs.min(360);
        }
        let sat = model.rng.borrow_mut().gen_range(0.5, 1.0);
        draw.polygon().points(pts).hsva(hue, sat, 1.0, 0.9);
        hue = model.rng.borrow_mut().gen_range(0.55, 0.7);
        prev_rad = base_rad;
        alpha -= model.rng.borrow_mut().gen_range(0.15, 0.25);
        alpha = alpha.max(0.1);
    }
    draw.background().rgb(0.75, 0.75, 0.8);
    sleep_ms(2000);
    draw.to_frame(app, &frame).unwrap();
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.caps.to_string()
                + ".png";
            app.main_window().capture_frame(out);
            model.caps += 1;
        }
        _ => {}
    }
}
