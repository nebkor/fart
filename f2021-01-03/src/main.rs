use chrono::Local;
use nannou::prelude::*;
use nannou::rand::rngs::StdRng;
use nannou::rand::seq::SliceRandom;
use nannou::rand::{Rng, SeedableRng};

use std::collections::{BTreeMap, HashSet};
use std::default::Default;

const NUM_DOTS: usize = 200;
const MIN_EX: f32 = 15.0;
const MAX_EX: f32 = 25.0;
const MAX_ITERATIONS: u32 = 30;
const RNG_SEED: u64 = 18;
const W_H: f32 = 900.0;

#[derive(Debug, Default)]
struct Dot {
    v_rad: f32,
    ex_rad: f32,
    pos: Vector2,
    hue: f32,
    value: f32,
}

#[derive(Debug)]
struct Model {
    dots: Vec<Dot>,
    active: HashSet<usize>,
    by_x: BTreeMap<i32, Vec<usize>>,
    by_y: BTreeMap<i32, Vec<usize>>,
    rng: StdRng,
    rect: Rect,
    line: bool,
    now: String,
    caps: usize,
}

impl Default for Model {
    fn default() -> Self {
        Model {
            dots: Vec::new(),
            active: HashSet::new(),
            by_x: BTreeMap::new(),
            by_y: BTreeMap::new(),
            rng: StdRng::seed_from_u64(RNG_SEED),
            rect: Rect::from_w_h(W_H, W_H),
            line: false,
            now: format!("{}", Local::now().format("%H:%M:%S")),
            caps: 0,
        }
    }
}

impl Model {
    pub fn new() -> Self {
        Model {
            dots: Vec::with_capacity(NUM_DOTS),
            ..Default::default()
        }
    }
    pub fn now(&self) -> &str {
        &self.now
    }

    pub fn neighbors(&self, candidate: &Dot) -> Vec<usize> {
        let mut x_set: HashSet<usize> = HashSet::new();
        let mut y_set: HashSet<usize> = HashSet::new();
        let c_x = candidate.pos.x;
        let c_y = candidate.pos.y;
        let radius = candidate.ex_rad;
        let min_x = (c_x - (1.2 * radius)).floor() as i32;
        let max_x = (c_x + (1.2 * radius)).ceil() as i32;
        let min_y = (c_y - (1.2 * radius)).floor() as i32;
        let max_y = (c_y + (1.2 * radius)).ceil() as i32;

        for (_, v) in self.by_x.range(min_x..=max_x) {
            for i in v.iter() {
                x_set.insert(*i);
            }
        }
        for (_, v) in self.by_y.range(min_y..=max_y) {
            for i in v.iter() {
                y_set.insert(*i);
            }
        }

        x_set
            .intersection(&y_set)
            .map(|e| e.to_usize().unwrap())
            .collect()
    }

    pub fn add_from_parent(&mut self, parent: usize) -> bool {
        let parent = &self.dots[parent];
        let mut child = Dot::default();
        let v_rad = 3.5;
        let ex_rad = v_rad + self.rng.gen_range(MIN_EX, MAX_EX);
        child.ex_rad = ex_rad;
        child.v_rad = v_rad;
        child.hue = 0.01;
        let mut count = 0;
        while count <= MAX_ITERATIONS {
            let rads = self.rng.gen_range(0.0, TAU);
            let scale = self.rng.gen::<f32>().sqrt();
            let radius = self.rng.gen_range(ex_rad, ex_rad * 1.5) * scale;
            let x = (rads.cos() * radius) + parent.pos.x;
            let y = (rads.sin() * radius) + parent.pos.y;
            child.pos = Vector2::new(x, y);
            let neighbors = self.neighbors(&child);
            if neighbors.is_empty() {
                break;
            }
            for i in neighbors.choose_multiple(&mut self.rng, neighbors.len()) {
                let n = &self.dots[*i];
                let d = n.pos - child.pos;
                let d = d.magnitude();
                let msd = ex_rad.max(n.ex_rad);
                if d < msd {
                    count += 1;
                    break;
                } else {
                    break;
                }
            }
        }
        if count < MAX_ITERATIONS {
            child.value = 1.0;
            let idx = self.dots.len();
            let x = child.pos.x as i32;
            let y = child.pos.y as i32;
            self.by_x.entry(x).or_insert(Vec::new()).push(idx);
            self.by_y.entry(y).or_insert(Vec::new()).push(idx);
            self.active.insert(idx);
            self.dots.push(child);
            true
        } else {
            false
        }
    }

    pub fn add(&mut self) {
        let hue: f32 = 0.627;
        let v_rad = 3.5;
        let ex_rad = self.rng.gen_range(MIN_EX, MAX_EX) + v_rad;
        let mut dot = Dot::default();
        dot.v_rad = v_rad;
        dot.ex_rad = ex_rad;
        dot.hue = hue;
        dot.value = 1.0;
        let idx = self.dots.len();
        loop {
            let rads = self.rng.gen_range(0.0, TAU);
            let radius = self.rng.gen::<f32>().sqrt() * 300.0;
            let x = radius * rads.cos();
            let y = radius * rads.sin();
            let pos = Vector2::new(x, y);
            dot.pos = pos.clone();
            let neighbors = self.neighbors(&dot);
            if neighbors.is_empty() {
                let x = x as i32;
                let y = y as i32;
                self.by_x.entry(x).or_insert(Vec::new()).push(idx);
                self.by_y.entry(y).or_insert(Vec::new()).push(idx);
                self.dots.push(dot);
                self.active.insert(idx);
                return;
            } else {
                for i in neighbors.choose_multiple(&mut self.rng, neighbors.len()) {
                    let n = &self.dots[*i];
                    let d = n.pos - pos;
                    let d = d.magnitude();
                    if d < ex_rad || d < n.ex_rad {
                        break;
                    } else {
                        let x = x as i32;
                        let y = y as i32;
                        self.by_x.entry(x).or_insert(Vec::new()).push(idx);
                        self.by_y.entry(y).or_insert(Vec::new()).push(idx);
                        self.dots.push(dot);
                        self.active.insert(idx);
                        return;
                    }
                }
            }
        }
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    app.new_window()
        .size(W_H as u32, W_H as u32)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    Model::new()
}

fn update(app: &App, model: &mut Model, update: Update) {
    let len = model.dots.len();
    if len < NUM_DOTS {
        let mut dels = HashSet::new();
        model.add();
        for i in model.active.clone().iter() {
            if !model.add_from_parent(*i) {
                dels.insert(*i);
            }
        }
        let new_active = model.active.difference(&dels).collect::<HashSet<_>>();
        model.active = new_active.iter().map(|e| e.to_usize().unwrap()).collect();
    } else {
        model.line = true;
        app.set_loop_mode(LoopMode::rate_fps(2.0))
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    if frame.nth() == 0 || app.keys.down.contains(&Key::B) {
        draw.background().rgb(0.9, 0.9, 0.9);
    }

    if model.line {
        draw.polyline()
            .weight(5.0)
            .join_round()
            .points(model.dots.iter().map(|d| d.pos).collect::<Vec<Vector2>>());
    } else {
        for d in model.dots.iter() {
            let hue = d.hue;
            let value = d.value;
            let radius = d.v_rad;
            let pos = d.pos;
            draw.ellipse()
                .x_y(pos.x, pos.y)
                .radius(radius)
                .hsv(hue, 1.0, value);
        }
    }

    draw.to_frame(app, &frame).unwrap();
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.caps.to_string()
                + ".png";
            app.main_window().capture_frame(out);
            model.caps += 1;
        }
        _ => {}
    }
}
