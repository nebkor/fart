## *Untitled*

You unlock this door with a Rust compiler. Beyond it is another dimension: a dimension of sound, a dimension of sight, a dimension of martinis. You're moving into a land of both cool cats and hot jazz, of color and form. You've just crossed over into... the Groovy Zone.

![f2021-01-05-2021_01_05_23_57_18-seed-18-1173.png](./f2021-01-05-2021_01_05_23_57_18-seed-18-1173.png)
