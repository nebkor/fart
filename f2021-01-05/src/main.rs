use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};

use std::cell::RefCell;

const HEIGHT: u32 = 2000;
const WIDTH: u32 = 1236;
const PHI: f32 = 1.61803;
const RNG_SEED: u64 = 18;
const COLOR_JITTER: f32 = 2.0; // +/- 15 degrees on the color wheel
const VAL_JITTER: f32 = 0.08 / 2.0;
const SAT_JITTER: f32 = 0.08 / 2.0;
const LEAN_JITTER: f32 = 4.0 / 2.0;
const POS_JITTER: f32 = 0.06 / 2.0; // this is a percentage applied to each Pane's height and width independently for x and y

use std::default::Default;

#[derive(Clone, Default, Debug)]
struct Pane {
    height: f32,
    hue: f32,
    sat: f32,
    value: f32,
    center: Vector2,
    lean: f32,
}

impl Pane {
    pub fn new(height: f32, hue: f32, sat: f32, value: f32, center: Vector2, lean: f32) -> Self {
        Pane {
            height,
            hue,
            sat,
            value,
            center,
            lean,
        }
    }

    pub fn width(&self) -> f32 {
        self.height / PHI
    }

    pub fn height(&self) -> f32 {
        self.height
    }

    pub fn center(&self) -> &Vector2 {
        &self.center
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    rng: StdRng,
    panes: Vec<Pane>,
    now: String,
    frame: RefCell<u64>,
}

impl Model {
    pub fn new() -> Self {
        let mut rng = StdRng::seed_from_u64(RNG_SEED);
        let panes = background(&mut rng);
        Model {
            rng,
            panes,
            now: format!("{}", Local::now().format("%Y_%m_%d_%H_%M_%S")),
            frame: RefCell::new(0),
        }
    }

    pub fn now(&self) -> &str {
        &self.now
    }
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();
    Model::new()
}

fn update(app: &App, model: &mut Model, _update: Update) {
    app.set_loop_mode(LoopMode::rate_fps(1.0));
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let fnum = frame.nth();

    if fnum == 0 || app.keys.down.contains(&Key::R) || app.keys.down.contains(&Key::B) {
        draw.background().color(BLACK);
    }

    for pane in model.panes.iter() {
        draw.rect()
            .height(pane.height())
            .width(pane.width())
            .x_y(pane.center().x, pane.center().y)
            .z_degrees(pane.lean)
            .hsva(pane.hue, pane.sat, pane.value, 0.8);
    }
    draw.to_frame(app, &frame).unwrap();

    *model.frame.borrow_mut() = fnum;
}

fn background(rng: &mut StdRng) -> Vec<Pane> {
    let mut height = top() - bottom();
    let base_hue_degs = 2.5; // reddish-orangeish
    let mut ret = Vec::new();
    let base_lean = 0.0;
    while height > 20.0 {
        let width = height * PHI;
        let hue = deg_to_rad(base_hue_degs + rng.gen_range(-COLOR_JITTER..=COLOR_JITTER));
        let val = 0.85 + rng.gen_range(-VAL_JITTER..=VAL_JITTER);
        let sat = 0.85 + rng.gen_range(-SAT_JITTER..=SAT_JITTER);
        let lean = base_lean + rng.gen_range(-LEAN_JITTER..=LEAN_JITTER);
        let x = rng.gen_range(-width * POS_JITTER..=width * POS_JITTER);
        let y = rng.gen_range(-height * POS_JITTER..=height * POS_JITTER);
        let center = Vector2::new(x, y);

        let pane = Pane::new(height, hue, sat, val, center, lean);

        ret.push(pane);

        height *= 0.8;
    }

    ret
}

fn grid(width: usize, height: usize) -> Vec<Vector2> {
    Vec::new()
}

fn top() -> f32 {
    HEIGHT as f32 * 0.9 / 2.0
}

fn bottom() -> f32 {
    HEIGHT as f32 * 0.9 / -2.0
}

fn left() -> f32 {
    WIDTH as f32 * 0.9 / -2.0
}

fn right() -> f32 {
    WIDTH as f32 * 0.9 / 2.0
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.frame.borrow().to_string()
                + ".png";
            app.main_window().capture_frame(out);
        }
        Key::R => {
            model.panes.clear();
            model.panes = background(&mut model.rng);
        }
        _ => {}
    }
}
