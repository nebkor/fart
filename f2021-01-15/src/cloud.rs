use nannou::geom::{Point2, Vector2};
use rand::Rng;

const SPEED_BASE: f32 = 1.5;
const SJIT: f32 = SPEED_BASE / 3.0;
const HUE_BASE: f32 = 250.0;

pub struct Puff {
    pub width: f32,
    pub height: f32,
    pub offsets: Vec<Vector2>,
    pub val: f32,
}

impl Puff {
    pub fn new(width: f32, height: f32, offsets: Vec<Vector2>, val: f32) -> Self {
        Puff {
            width,
            height,
            offsets,
            val,
        }
    }

    pub fn offset(&self) -> Vector2 {
        self.offsets.iter().fold(Vector2::default(), |v, &o| v + o)
    }
}

pub struct Cloud {
    pub id: u64,
    pub width: f32,
    pub height: f32,
    pub center: Point2,
    pub speed: f32,
    pub hue_deg: f32,
    pub puffs: Vec<Puff>,
}

impl Cloud {
    pub fn new(width: f32, ratio: f32, center: Point2, id: u64, rng: &mut impl Rng) -> Self {
        let layers = rng.gen_range(2..=4);
        let part_rat = ratio / 8.0;
        let rat = ratio + rng.gen_range(-part_rat..part_rat);
        let height = width * rat;

        let mut puffs = Vec::new();
        let mut offsets = Vec::new();
        let mut w: f32 = width;

        for n in (1..=layers).rev() {
            let pw = w / (layers - n).max(1) as f32;
            let val = n as f32 / layers as f32;
            let xjit = pw / 1.5;
            let yjit = xjit * rat;
            let pw = pw + rng.gen_range(-xjit..xjit);
            let mut x_min = f32::MAX;
            let mut x_max = f32::MIN;
            let mut y_min = f32::MAX;
            let mut y_max = f32::MIN;
            let ph = pw * rat;
            for _i in 0..n {
                let mut offsets = offsets.clone();
                let x = rng.gen_range(-xjit..xjit);
                let y = rng.gen_range(-yjit..yjit);
                x_min = x_min.min(x);
                x_max = x_max.max(x);
                y_min = y_min.min(y);
                y_max = y_max.max(y);
                offsets.push(Vector2::new(x, y));
                let puff = Puff::new(pw, ph, offsets, val);
                puffs.push(puff);
            }
            w = x_max - x_min;
            let h = y_max - y_min;
            offsets.push(Vector2::new(w / 2.0, h / 2.0));
        }
        let speed = rng.gen_range(-1..=1) as f32 * SPEED_BASE + rng.gen_range(-SJIT..SJIT);
        let hue_deg = HUE_BASE + rng.gen_range(-10.0..10.0);

        Cloud {
            id,
            width,
            height,
            center,
            speed,
            hue_deg,
            puffs,
        }
    }

    pub fn update(&mut self) {
        self.center.x += self.speed;
    }

    pub fn center(&self) -> Point2 {
        self.center
    }
}
