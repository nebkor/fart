use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};

use fartlib::*;

mod cloud;
use cloud::*;

const WIDTH: u32 = 1000;
const HEIGHT: u32 = 1000;

const RNG_SEED: u64 = 18;

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    rng: StdRng,
    clouds: Vec<Cloud>,
    sky: GRect,
    num_clouds: usize,
    now: String,
    nth_frame: u64,
    record: bool,
    restart: bool,
}

fn model(app: &App) -> Model {
    // app.set_loop_mode(LoopMode::loop_once());
    let _window = app
        .new_window()
        .view(view)
        .size(WIDTH, HEIGHT)
        .build()
        .unwrap();

    let rect = app.main_window().rect();
    let cfloor = 0.5 * rect.h() - rect.h() / 2.0;
    let width = rect.w();
    let height = rect.h();
    let hjit = height * 0.2;

    let sky = GRect::new(width, height, Point2::new(0.0, 0.0), GOrient::Vertical(10));
    let mut rng = StdRng::seed_from_u64(RNG_SEED);
    let num_clouds = rng.gen_range(2..=4);
    let mut clouds = Vec::with_capacity(num_clouds);
    for c in 0..num_clouds {
        let w = WIDTH as f32 / (num_clouds as f32);
        let ratio = rng.gen_range(0.2..1.8);
        let y = cfloor + rng.gen_range(-hjit..hjit);
        let wjit = w / 2.0;
        let x = -500.0 + (w / 2.0) + (c as f32 * w / 1.5) + rng.gen_range(-wjit..wjit);
        let cloud = Cloud::new(w, ratio, Point2::new(x, y), c as u64, &mut rng);
        clouds.push(cloud);
    }

    Model {
        rng,
        sky,
        clouds,
        num_clouds,
        now: format!("{}", Local::now().format("%Y%m%d_%H:%M:%S")),
        nth_frame: 0,
        record: false,
        restart: false,
    }
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    for cloud in model.clouds.iter_mut() {
        cloud.update();
    }
    model.nth_frame += 1;
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    draw.background().color(BLACK);

    let pcs = model.sky.points_colored(sky_gradient);

    draw.polygon().points_colored(pcs);

    for cloud in model.clouds.iter() {
        let center = cloud.center();
        let hue = deg_to_pct(cloud.hue_deg);
        for puff in cloud.puffs.iter() {
            let p = center + puff.offset;
            let val = puff.val;
            let height = puff.height;
            let width = puff.width;
            draw.ellipse()
                .hsva(hue, 0.1, val, 0.8)
                .width(width)
                .height(height)
                .xy(p);
        }
    }

    draw.to_frame(app, &frame).unwrap();

    if app.keys.down.contains(&Key::S) {
        app.main_window().capture_frame("out.png");
    }
}

fn sky_gradient(y: f32, min: f32, max: f32) -> Hsva {
    // the larger the exp, the flatter the curve, so the gradient is more subtle (and ramps up away from 0 at the beginning of it more aggressively)
    let exp = 2.5;
    // map to percent of span to avoid working with small floating point numbers until the very last step
    let s = map_range(y, min, max, 0.0, 100.0);
    // map percent to range of log domain between a smidge over 0 and 1 (log(exp) == 1.0)
    let s = map_range(s, 0.0, 100.0, 1.2, exp);
    let s = 1.0 - 1.0 / s.powf(exp);
    Hsva::new(210.0, s, 1.0, 1.0)
}

fn deg_to_pct(d: f32) -> f32 {
    d / 360.0
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + &model.now
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.nth_frame.to_string()
                + ".png";

            app.main_window().capture_frame(out);
        }
        Key::R => {
            model.restart = true;
        }
        Key::V => {
            model.record = !model.record;
        }
        _ => {}
    }
}
