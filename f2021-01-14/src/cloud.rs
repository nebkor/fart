use nannou::geom::{Point2, Vector2};
use rand::rngs::StdRng;
use rand::Rng;

const SPEED_BASE: f32 = 1.0;
const SJIT: f32 = SPEED_BASE / 3.0;
const HUE_BASE: f32 = 250.0;

pub struct Puff {
    pub width: f32,
    pub height: f32,
    pub offset: Vector2,
    pub val: f32,
}

impl Puff {
    pub fn new(width: f32, height: f32, offset: Vector2, val: f32) -> Self {
        Puff {
            width,
            height,
            offset,
            val,
        }
    }
}

pub struct Cloud {
    id: u64,
    layers: usize,
    pub width: f32,
    pub height: f32,
    center: Point2,
    speed: f32,
    pub hue_deg: f32,
    pub puffs: Vec<Puff>,
}

impl Cloud {
    pub fn new(width: f32, ratio: f32, center: Point2, id: u64, rng: &mut StdRng) -> Self {
        let layers = rng.gen_range(2..=4);
        let part_rat = ratio / 3.0;
        let rat = ratio + rng.gen_range(-part_rat..part_rat);
        let height = width * rat;

        let mut puffs = Vec::new();
        let mut w: f32 = width;
        for n in (1..=layers).rev() {
            let val = n as f32 / layers as f32;
            let pw = w / n as f32;
            let xjit = pw / 4.0;
            let yjit = xjit * rat;
            let pw = pw + rng.gen_range(-xjit..xjit);
            w = w - pw;
            let w_2 = w / 2.0;
            let x_min = center.x - w_2;
            let ph = pw / rat;
            for i in 0..n {
                let x = x_min + (i as f32 * pw) + rng.gen_range(-xjit..xjit);
                let y = center.y + rng.gen_range(-yjit..yjit);
                let puff = Puff::new(pw, ph, Vector2::new(x, y), val);
                puffs.push(puff);
            }
        }
        let speed = rng.gen_range(-1..=1) as f32 * SPEED_BASE + rng.gen_range(-SJIT..SJIT);
        let hue_deg = HUE_BASE + rng.gen_range(-10.0..10.0);

        Cloud {
            id,
            layers,
            width,
            height,
            center,
            speed,
            hue_deg,
            puffs,
        }
    }

    pub fn update(&mut self) {
        self.center.x += self.speed;
    }

    pub fn center(&self) -> Point2 {
        self.center
    }
}
