use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};

const WIDTH: u32 = 1200;

const RNG_SEED: u64 = 18;

const COLOR_BASE: f32 = 240.0; // degrees, blue
const COLOR_JIT: f32 = 8.0; // +/- 15 degrees on the color wheel

const FLOOR_BASE: f32 = 50.0;
const FLOOR_JIT: f32 = 25.0;
const CEIL_BASE: f32 = WIDTH as f32 / 2.4;
const CEIL_JIT: f32 = CEIL_BASE * 0.2;
const ARC_JIT: f32 = 6.0; // how wide an arc a point can move

const VAL_JIT: f32 = 0.2;
const VAL_BASE: f32 = 1.0 - VAL_JIT;

const SAT_JIT: f32 = 0.3;
const SAT_BASE: f32 = 1.0 - SAT_JIT;

const MIN_SEG: f32 = 5.0; // length  of smallest segment

const AGE_MUL: usize = 20;

use std::default::Default;

#[derive(Clone, Default, Debug)]
struct Strike {
    ceil: f32,
    floor: f32,
    points: Vec<Point2>,
    hue: f32,
    sat: f32,
    value: f32,
    alpha: f32,
    age: usize,
    start: f32, // degrees around circle
    dps: f32,
    cw: bool, // clockwise?
}

impl Strike {
    pub fn new(
        ceil: f32,
        floor: f32,
        start: f32,
        hue: f32,
        sat: f32,
        value: f32,
        alpha: f32,
        dps: f32,
        cw: bool,
    ) -> Self {
        let mut points = Vec::new();
        let rads = deg_to_rad(start);
        let p1x = rads.cos() * floor;
        let p1y = rads.sin() * floor;
        let p1 = Point2::new(p1x, p1y);
        let p2x = rads.cos() * ceil;
        let p2y = rads.sin() * ceil;
        let p2 = Point2::new(p2x, p2y);
        points.push(p1);
        points.push(p2);

        Strike {
            ceil,
            floor,
            hue,
            points,
            sat,
            value,
            alpha,
            age: 0,
            start,
            dps,
            cw,
        }
    }

    pub fn update_step(&mut self) {
        self.age += 1;
        let ang = if self.cw {
            self.start + self.age as f32 * self.dps
        } else {
            self.start - self.age as f32 * self.dps
        };
        let rads = deg_to_rad(ang);
        self.points[0].x = self.floor * rads.cos();
        self.points[0].y = self.floor * rads.sin();
        self.points[1].x = self.ceil * rads.cos();
        self.points[1].y = self.ceil * rads.sin();
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

#[derive(Default, Debug)]
struct Core {
    r: f32,
    h: f32,
    s: f32,
    v: f32,
    a: f32,
}

impl Core {
    pub fn new(strikes: &Vec<Strike>) -> Self {
        let mut h = 0.0;
        let mut sat = 0.0;
        let mut v = 0.0;
        let mut a = 0.0;
        let mut l = 0.0;
        let mut r = f32::MIN;
        for s in strikes.iter() {
            h += s.hue;
            sat += s.sat;
            v += s.value;
            a += s.alpha;
            r = r.max(s.floor);
        }
        let div = strikes.len() as f32;
        h /= div;
        h -= 240.0;
        sat /= div;
        v /= div;
        a /= div;
        Core { h, s: sat, v, a, r }
    }
}

struct Model {
    rng: StdRng,
    strikes: Vec<Strike>,
    core: Core,
    now: String,
    frame: u64,
    record: bool,
    reset: bool,
}

fn mk_hue(rng: &mut impl Rng) -> f32 {
    let h = COLOR_BASE + rng.gen_range(-COLOR_JIT..COLOR_JIT);
    h / 360.0
}

impl Model {
    pub fn new() -> Self {
        let mut rng = StdRng::seed_from_u64(RNG_SEED);
        let num_strikes = rng.gen_range(18..22) as usize;
        let strikes = mk_strikes(num_strikes, &mut rng);
        let core = Core::new(&strikes);
        Model {
            rng,
            strikes,
            core,
            now: format!("{}", Local::now().format("%Y%m%d_%H:%M:%S")),
            frame: 0,
            record: false,
            reset: false,
        }
    }

    pub fn now(&self) -> &str {
        &self.now
    }
}

fn mk_strikes(n: usize, rng: &mut impl Rng) -> Vec<Strike> {
    let mut strikes = Vec::new();

    for _ in 0..n {
        let ceil = CEIL_BASE + rng.gen_range(-CEIL_JIT..CEIL_JIT);
        let floor = FLOOR_BASE + rng.gen_range(-FLOOR_JIT..FLOOR_JIT);
        let hue = rng.gen_range(210.0..310.0);
        let sat = SAT_BASE + rng.gen_range(-SAT_JIT..SAT_JIT);
        let val = VAL_BASE + rng.gen_range(-VAL_JIT..VAL_JIT);
        let alpha = 0.6 + rng.gen_range(-0.4..0.4);
        let ang = rng.gen_range(0.0..360.0);
        let dps = rng.gen_range(0.4..1.2);
        let strike = Strike::new(ceil, floor, ang, hue, sat, val, alpha, dps, rng.gen());
        strikes.push(strike);
    }

    strikes
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(WIDTH, WIDTH)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();
    Model::new()
}

fn update(app: &App, model: &mut Model, _update: Update) {
    model.frame = app.elapsed_frames();

    if model.reset {
        model.reset = false;
        let num_strikes = model.rng.gen_range(27..35);
        let strikes = mk_strikes(num_strikes, &mut model.rng);
        model.strikes = strikes;
        return;
    }

    let mut logans = Vec::new();
    for (i, strike) in model.strikes.iter_mut().enumerate() {
        strike.update_step();
        if strike.age as i32 > 100 + model.rng.gen_range(-50..50) {
            logans.push(i);
        }
    }
    let new_strikes = mk_strikes(logans.len(), &mut model.rng);
    for (i, s) in logans.iter().zip(new_strikes.into_iter()) {
        model.strikes[*i] = s;
    }
    let core = Core::new(&model.strikes);
    dbg!(&core);
    model.core = core;
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let fnum = frame.nth();

    if fnum == 0 || app.keys.down.contains(&Key::R) || app.keys.down.contains(&Key::B) {
        draw.background().color(BLACK);
    }

    let rect = app.main_window().rect();

    // background
    draw.rect().wh(rect.wh()).hsva(d2h(25.0), 0.05, 0.0, 0.1);

    for s in model.strikes.iter() {
        let h = d2h(s.hue);
        draw.line()
            .weight(16.0)
            .caps_round()
            .points(s.points[0], s.points[1])
            .hsva(h, s.sat, s.value, s.alpha);
    }

    let c = &model.core;
    draw.ellipse()
        .radius(c.r)
        .x_y(0.0, 0.0)
        .hsva(d2h(c.h), c.s, c.v, c.a);

    draw.to_frame(app, &frame).unwrap();

    if model.record {
        let out = format!("frames-{}/{:04}.png", model.now(), frame.nth());
        app.main_window().capture_frame(out);
    }
}

// this is maybe "degress to turns", though even then it's resetting at 360, so idunno
fn d2h(d: f32) -> f32 {
    (d % 360.0) / 360.0
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.frame.to_string()
                + ".png";
            app.main_window().capture_frame(out);
        }
        Key::V => {
            model.record = !model.record;
        }

        Key::R => {
            model.reset = true;
        }
        _ => {}
    }
}
