# untitled

# Initial idea
Colored radial lines that rotate. Originally, I was thinking that the lines would be segmented in
some way, and was thinking of lightning strikes. I was also thinking about having the radial
velocity be non-linear, so that you'd get shearing and spirals. But, this just has straight spokes
that rotate with different speeds and directions, randomly spawning and dying.

I think went OK!

![tempit fugus](./2216.png)
