use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};

use std::default::Default;

const PHI: f32 = 1.61803399;

const WIDTH: u32 = 800;

const RNG_SEED: u64 = 18;

// lightning strike/flame stuff
const COLOR_BASE: f32 = 225.0; // degrees, blue
const COLOR_JIT: f32 = 15.0; // +/- degrees on the color wheel
const STRIKE_BASE: f32 = 80.0;
const STRIKE_JIT: f32 = 10.0;
const HEIGHT_BASE: f32 = WIDTH as f32 / 2.0;
const HEIGHT_JIT: f32 = HEIGHT_BASE * 0.2;

// firefly stuff
const NUM_FLIES: usize = 70;
const FLY_RAD: f32 = 3.0;
const HALO: f32 = FLY_RAD * 1.8;
const SPD_MIN: f32 = 9.0;
const SPD_MAX: f32 = 12.0;
const FLY_COLOR: f32 = 55.0; // degrees, orange

const MIN_SEG: f32 = 3.0; // length  of smallest segment

#[derive(Debug, Default, Clone)]
struct SPoint {
    p: Point2,
    offset: f32,
}

#[derive(Clone, Default, Debug)]
struct Strike {
    height: f32,
    bottom: f32,
    width: f32,
    points: Vec<SPoint>,
    hue: f32,
    sat: f32,
    value: f32,
    alpha: f32,
    age: usize,
    lifespan: usize,
    strength: f32,
}

impl Strike {
    pub fn new(
        height: f32,
        bottom: f32,
        width: f32,
        hue: f32,
        sat: f32,
        value: f32,
        alpha: f32,
    ) -> Self {
        let mut points = Vec::new();

        let w2 = width / 2.0;
        let mut h = 0.0;
        let mut dy = height / PHI;
        let mut dx = -w2;
        while h < height * 1.1 {
            let x = dx;
            let y = h;
            let p = Point2::new(x, y);
            points.push(SPoint { p, offset: 0.0 });
            dx /= PHI;
            h += dy;
            dy = (dy / PHI).max(MIN_SEG);
        }
        let mid = points.len() as i32;
        points.push(SPoint {
            p: Point2::new(0.0, h + MIN_SEG),
            offset: 0.0,
        });
        let height = h;
        for n in (0..mid).rev() {
            let i = (mid - (mid - n).abs()) as usize;
            let x = -points[i].p.x;
            let y = points[i].p.y;
            let offset = points[i].offset;
            let p = Point2::new(x, y);
            points.push(SPoint { p, offset });
        }

        Strike {
            height,
            bottom,
            width,
            hue,
            points,
            sat,
            value,
            alpha,
            age: 0,
            lifespan: 240,
            strength: 0.0,
        }
    }

    pub fn hue(&self) -> f32 {
        d2h(self.hue)
    }

    pub fn update_step(&mut self, rng: &mut impl Rng) {
        self.age += 1;
        let peak = self.lifespan as f32 / 2.0;
        let strength = ((peak - (peak - self.age as f32).abs()) / peak).powi(3);
        self.strength = strength;

        let mid = self.points.len() / 2;
        for i in 1..self.points.len() - 1 {
            let os = if i < mid {
                self.points[i].offset + rng.gen_range(-3.0..3.0)
            } else {
                let mid = mid as i32;
                let i = i as i32;
                let i = mid - (i - mid).abs();
                self.points[i as usize].offset
            };
            self.points[i].offset = os;
        }
    }

    pub fn points(&self, scale: f32) -> Vec<Point2> {
        let mut offsets = Vec::new();
        let mid = self.points.len() / 2;
        let mut offset = 0.0;
        for i in 0..self.points.len() {
            let os = if i <= mid {
                offset += self.points[i].offset;
                offset
            } else {
                let mid = mid as i32;
                let i = i as i32;
                let i = mid - (i - mid).abs();
                offsets[i as usize]
            };
            offsets.push(os);
        }

        self.points
            .iter()
            .zip(offsets.iter())
            .map(|(p, o)| {
                let x = (scale * p.p.x) + o;
                let y = (p.p.y * self.strength.powf(PHI)) + self.bottom;
                Point2::new(x, y)
            })
            .collect()
    }
}

#[derive(Clone, Debug, Default)]
struct FireFly {
    p: Point2,
    v: Vector2,
    h: f32,
    id: u64,
}

impl FireFly {
    pub fn new(p: Point2, v: Vector2, h: f32, id: u64) -> Self {
        FireFly { p, v, h, id }
    }

    pub fn update(&mut self, flame: &Strike, flies: &[FireFly]) {
        // update the velocity to:
        // 1) move toward the center (it's cool to be casual)
        // 2) move away from other flies (not too urgently)
        // 3) move away from the flame (very urgently)
        //
        let c = Point2::new(0.0, -150.0);
        let to_center = c - self.p;
        let ts = to_center.magnitude() * 0.7;
        let to_center = to_center.normalize() * ts;

        let mut misanthropy = Vector2::default();
        for f in flies.iter() {
            if f.id == self.id {
                continue;
            }
            let v = self.p - f.p;
            let d = 0.5 * v.magnitude().powf(0.2);
            misanthropy += v.normalize() * d;
        }

        let mut fire_bad = Vector2::default();
        for p in flame.points(1.0).iter() {
            let v = *p - self.p;
            let d = 1.1 * v.magnitude().powf(0.5);
            fire_bad -= v.normalize() * d;
        }

        let v = self.v + misanthropy + fire_bad + to_center;
        let s = v.magnitude().min(SPD_MAX).max(SPD_MIN);
        self.v = v.normalize() * s;
        self.p += self.v;
    }

    pub fn draw(&self, draw: &Draw) {
        let h = d2h(self.h);
        draw.ellipse()
            .xy(self.p)
            .radius(HALO)
            .hsva(h, 0.3, 1.0, 0.2);
        draw.ellipse()
            .xy(self.p)
            .radius(FLY_RAD)
            .hsva(h, 0.9, 1.0, 0.7);
    }
}

struct Model {
    rng: StdRng,
    strike: Strike,
    flies: Vec<FireFly>,
    now: String,
    frame: u64,
    record: bool,
    reset: bool,
}

impl Model {
    pub fn new() -> Self {
        let mut rng = StdRng::seed_from_u64(RNG_SEED);
        let strike = mk_strike(&mut rng);
        let flies = mk_flies(&mut rng);
        Model {
            rng,
            strike,
            flies,
            now: format!("{}", Local::now().format("%Y%m%d_%H:%M:%S")),
            frame: 0,
            record: false,
            reset: false,
        }
    }

    pub fn now(&self) -> &str {
        &self.now
    }

    pub fn update_flies(&mut self) {
        for i in 0..self.flies.len() {
            let mut f = self.flies[i].clone();
            f.update(&self.strike, &self.flies);
            self.flies[i] = f;
        }
    }
}

//------------------------------------------------------------------------------------------
// mandatory functions
//
fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(WIDTH, WIDTH)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();
    Model::new()
}

fn update(app: &App, model: &mut Model, _update: Update) {
    model.frame = app.elapsed_frames();
    model.strike.update_step(&mut model.rng);

    let s = &model.strike;
    if model.reset || s.age > s.lifespan {
        model.reset = false;
        let strike = mk_strike(&mut model.rng);
        model.strike = strike;
    }

    model.update_flies();
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let fnum = frame.nth();

    if fnum == 0 || app.keys.down.contains(&Key::R) || app.keys.down.contains(&Key::B) {
        draw.background().color(BLACK);
    }

    let rect = app.main_window().rect();

    // background
    let s = &model.strike;
    if s.age == 0 {
        //draw.background().hsva(0.0, 0.1, 0.001, 0.02);
        draw.background().color(DIMGRAY);
    }
    draw.rect().wh(rect.wh()).hsv(s.hue(), 0.7, 0.4);
    let bwidth = (WIDTH - 200) as f32;
    draw.rect().w_h(bwidth, bwidth).hsv(s.hue(), 0.1, 0.0);

    draw_strike(&draw, &s);

    for fly in model.flies.iter() {
        fly.draw(&draw);
    }

    draw.to_frame(app, &frame).unwrap();

    if model.record {
        let out = format!("frames-{}/{:04}.png", model.now(), frame.nth());
        app.main_window().capture_frame(out);
    }
}

//--------------------------------------------------------------------------------------------
// local utility functions
//
fn draw_strike(draw: &Draw, s: &Strike) {
    let points = s.points(1.0);
    draw.polygon()
        .points(points)
        .hsva(s.hue(), s.sat, s.value, s.alpha);

    draw.polygon()
        .points(s.points(0.4))
        .hsva(s.hue(), 0.3, 0.8, 0.5);

    draw.polygon()
        .points(s.points(0.25))
        .hsva(s.hue(), 0.2, 0.9, 0.6);

    draw.polygon()
        .points(s.points(0.15))
        .hsva(s.hue(), 0.1, 1.0, 0.7);
}

fn mk_strike(rng: &mut impl Rng) -> Strike {
    let height = HEIGHT_BASE + rng.gen_range(-HEIGHT_JIT..HEIGHT_JIT);
    let width = STRIKE_BASE + rng.gen_range(-STRIKE_JIT..STRIKE_JIT);
    let bottom = (WIDTH - 200) as f32 / -2.0;
    let hue = rng.gen_range(0.0..360.0); // mk_hue_degs(rng);
    let sat = 1.0;
    let value = 1.0;
    let alpha = 1.0;
    Strike::new(height, bottom, width, hue, sat, value, alpha)
}

fn mk_flies(rng: &mut impl Rng) -> Vec<FireFly> {
    let mut flies = Vec::new();
    for i in 0..NUM_FLIES {
        let span = (WIDTH - 200) as f32 / 2.0;
        let x = rng.gen_range(-span..span);
        let y = rng.gen_range(-span..span);
        let v = Vector2::default();
        let p = Point2::new(x, y);
        let h = FLY_COLOR + rng.gen_range(-COLOR_JIT..COLOR_JIT);
        let fly = FireFly::new(p, v, h, i as u64);
        flies.push(fly);
    }
    flies
}

// this is maybe "degress to turns", though even then it's resetting at 360, so idunno
fn d2h(d: f32) -> f32 {
    (d % 360.0) / 360.0
}

fn mk_hue_degs(rng: &mut impl Rng) -> f32 {
    COLOR_BASE + rng.gen_range(-COLOR_JIT..COLOR_JIT)
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.frame.to_string()
                + ".png";
            app.main_window().capture_frame(out);
        }
        Key::V => {
            model.record = !model.record;
        }

        Key::R => {
            model.reset = true;
        }
        _ => {}
    }
}
