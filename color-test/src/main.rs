use nannou::prelude::*;

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    grect: GRect,
}

enum Gorient {
    Vertical(usize),
    Horizontal(usize),
}

struct GRect {
    w: f32,
    h: f32,
    center: Point2,
    orient: Gorient,
}

impl GRect {
    pub fn new(w: f32, h: f32, center: Point2, orient: Gorient) -> Self {
        GRect {
            w,
            h,
            center,
            orient,
        }
    }

    pub fn points_colored<F>(&self, color_fn: F) -> Vec<(Point2, Hsva)>
    where
        F: Fn(f32, f32, f32) -> Hsva,
    {
        let w_2 = self.w / 2.0;
        let h_2 = self.h / 2.0;
        let x_max = self.center.x + w_2;
        let x_min = self.center.x - w_2;
        let y_max = self.center.y + h_2;
        let y_min = self.center.y - h_2;
        let mut points: Vec<(Point2, Hsva)>;
        match self.orient {
            Gorient::Vertical(steps) => {
                points = Vec::with_capacity(2 * (self.h as usize / steps) + 1);
                let dy = self.h / (steps as f32 - 1.0);
                let mut y = y_min;
                for i in 0..steps {
                    y = y_min + (i as f32 * dy);
                    y = y.min(y_max);
                    let c = color_fn(y, y_min, y_max);
                    let p = Point2::new(x_min, y);
                    points.push((p, c));
                }
                println!("y: {}, y_max: {}, dy: {}", y, y_max, dy);
                for i in (0..steps).rev() {
                    y = y_min + (i as f32 * dy);
                    y = y.max(y_min);
                    let c = color_fn(y, y_min, y_max);
                    let p = Point2::new(x_max, y);
                    points.push((p, c));
                }
            }
            Gorient::Horizontal(steps) => {
                points = Vec::with_capacity((self.h as usize / steps) + 1);
                let dx = self.w / steps as f32;
                let mut x = x_min;
                for i in 0..steps {
                    x = x_min + (i as f32 * dx);
                    x = x.min(x_max);
                    let c = color_fn(x, x_min, x_max);
                    let p = Point2::new(x, y_min);
                    points.push((p, c));
                }
                for i in (0..steps).rev() {
                    x = x_min + (i as f32 * dx);
                    x = x.max(x_min);
                    let c = color_fn(x, x_min, x_max);
                    let p = Point2::new(x, y_max);
                    points.push((p, c));
                }
            }
        }
        points
    }
}

fn model(app: &App) -> Model {
    let _window = app.new_window().view(view).size(850, 700).build().unwrap();
    let grect = GRect::new(250.0, 600.0, Point2::new(0.0, 0.0), Gorient::Vertical(5));
    app.set_loop_mode(LoopMode::loop_once());
    Model { grect }
}

fn update(app: &App, model: &mut Model, _update: Update) {}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    draw.background().color(BLACK);

    let pcs = model.grect.points_colored(sky_gradient);

    draw.polygon().points_colored(pcs);

    draw.to_frame(app, &frame).unwrap();

    if app.keys.down.contains(&Key::S) {
        app.main_window().capture_frame("out.png");
    }
}

fn sky_gradient(y: f32, min: f32, max: f32) -> Hsva {
    let exp = 2.1;
    // map to percent of span
    let s = map_range(y, min, max, 0.0, 100.0);
    // map percent to range of log domain between 0 and 1
    let s = map_range(s, 0.0, 100.0, 1.0, exp);
    let s = 1.2 - 1.0 / s.powf(exp);
    dbg!(s);
    Hsva::new(240.0, s, 1.0, 1.0)
}

fn deg_to_pct(d: f32) -> f32 {
    d / 360.0
}
