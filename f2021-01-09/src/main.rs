use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};

const ORB_RAD: f32 = 90.0;
const PHI: f32 = 0.9; // lies

const NUM_BALLS: usize = 5;
const BALL_COLOR_BASE: f32 = 25.0;
const BALL_COLOR_JIT: f32 = 10.0;

const WANDER: f32 = 500.0;

const WIDTH: u32 = 1200;
const HEIGHT: u32 = 1200;

const RNG_SEED: u64 = 18;

#[derive(Clone, Default, Debug)]
struct Pane {
    major: f32,
    hue: f32,
    sat: f32,
    value: f32,
    center: Vector2,
    tall: bool,
}

impl Pane {
    pub fn new(major: f32, hue: f32, sat: f32, value: f32, center: Vector2, tall: bool) -> Self {
        Pane {
            major,
            hue,
            sat,
            value,
            center,
            tall,
        }
    }

    pub fn width(&self) -> f32 {
        if !self.tall {
            self.major
        } else {
            self.major / PHI
        }
    }

    pub fn height(&self) -> f32 {
        if self.tall {
            self.major
        } else {
            self.major / PHI
        }
    }

    pub fn center(&self) -> &Vector2 {
        &self.center
    }
}

#[derive(Debug, Default)]
struct Ball {
    p: Point2,
    v: Vector2,
    h: f32,
    s: f32,
}

impl Ball {
    pub fn new(p: Point2, v: Vector2, h: f32, s: f32) -> Self {
        Ball { p, v, h, s }
    }

    pub fn update_center(&mut self, w: f32, h: f32, rng: &mut StdRng) {
        self.p += self.v;
        let h_2 = h / 2.0;
        let w_2 = w / 2.0;
        let x_d = self.p.x.abs();
        let y_d = self.p.y.abs();
        if x_d > (w - 10.0) / 2.0 {
            let sign = self.p.x / x_d;
            self.p.x = sign * (w_2 - 15.0);
            self.v.x *= -1.0;
            let dy = self.v.y.abs() / 4.0;
            self.v.y = self.v.y + rng.gen_range(-dy..dy);
        }
        if self.p.y.abs() > h / 2.0 {
            let sign = self.p.y / y_d;
            self.p.y = sign * (h_2 - 15.0);
            self.v.y *= -1.0;
            let dx = self.v.x.abs() / 4.0;
            self.v.x = self.v.x + rng.gen_range(-dx..dx);
        }
        self.v *= 2.0;
        self.v = self.v.limit_magnitude(w / 140.0);
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    rng: StdRng,
    bg: Vec<Pane>,
    balls: Vec<Ball>,
    now: String,
    frame: u64,
}

impl Model {
    pub fn new() -> Self {
        let mut rng = StdRng::seed_from_u64(RNG_SEED);
        let bg = background(WIDTH as f32, HEIGHT as f32, &mut rng);
        let mut balls = Vec::with_capacity(NUM_BALLS);

        for _ in 0..NUM_BALLS {
            let x = rng.gen_range(-WANDER..WANDER);
            let y = rng.gen_range(-WANDER..WANDER);
            let theta = rng.gen_range(0.0..TAU);
            let mag = rng.gen_range(5.0..12.0);
            let vx = mag * theta.cos();
            let vy = mag * theta.sin();
            let h = d2h(BALL_COLOR_BASE + rng.gen_range(-BALL_COLOR_JIT..BALL_COLOR_JIT));
            let s = rng.gen_range(0.1..0.6);
            balls.push(Ball::new(Point2::new(x, y), Vector2::new(vx, vy), h, s));
        }

        Model {
            rng,
            bg,
            balls,
            now: format!("{}", Local::now().format("%Y_%m_%d_%H_%M_%S")),
            frame: 0,
        }
    }

    pub fn now(&self) -> &str {
        &self.now
    }
}

fn mk_balls(n: i32, rng: &mut StdRng) -> Vec<Ball> {
    let mut balls = Vec::with_capacity(n as usize);
    for _ in 0..n {
        let x = rng.gen_range(-WANDER..WANDER);
        let y = rng.gen_range(-WANDER..WANDER);
        let theta = rng.gen_range(0.0..TAU);
        let mag = rng.gen_range(5.0..12.0);
        let vx = mag * theta.cos();
        let vy = mag * theta.sin();
        let h = d2h(BALL_COLOR_BASE + rng.gen_range(-BALL_COLOR_JIT..BALL_COLOR_JIT));
        let s = rng.gen_range(0.1..0.6);
        balls.push(Ball::new(Point2::new(x, y), Vector2::new(vx, vy), h, s));
    }
    balls
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();
    Model::new()
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let rect = app.main_window().rect();
    let (w, h) = rect.w_h();
    if model.frame % 20 == 0 {
        model.bg = background(w, h, &mut model.rng);
    }
    model.frame = app.elapsed_frames();
    for b in model.balls.iter_mut() {
        b.update_center(w, h, &mut model.rng);
    }

    let cur_balls = model.balls.len() as i32;

    let num_balls = round(w / (1.5 * ORB_RAD)).max(NUM_BALLS as i32);
    let bdiff = cur_balls - num_balls;

    if bdiff < 0 {
        let mut balls = mk_balls(bdiff.abs(), &mut model.rng);
        model.balls.append(&mut balls);
    } else if bdiff > 0 {
        model.balls.truncate(num_balls as usize);
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    if app.keys.down.contains(&Key::B) {
        draw.background().color(BLACK);
    }

    draw.rect()
        .wh(app.window_rect().wh())
        .hsva(0.0, 0.0, 0.0, 0.15);

    for p in model.bg.iter() {
        let rot = if p.tall { -90.0 } else { 90.0 };
        let rot = deg_to_rad(rot);
        draw.tri()
            .height(p.height())
            .width(p.width())
            .x_y(p.center().x, p.center().y)
            .rotate(rot)
            .hsva(p.hue, p.sat, p.value, 0.4);
    }

    let rings = 5;
    let delta = ORB_RAD / ((rings - 1).max(1)) as f32;
    for b in model.balls.iter() {
        for r in (1..=rings).rev() {
            let r = r as f32 * delta;
            draw.ellipse()
                .radius(r)
                .x_y(b.p.x, b.p.y)
                .hsva(b.h, b.s, 0.7, 0.3);
        }
    }

    // let out = format!("frames/{:04}.png", frame.nth());
    // app.main_window().capture_frame(out);

    draw.to_frame(app, &frame).unwrap();
}

fn d2h(d: f32) -> f32 {
    d / 360.0
}

fn background(w: f32, h: f32, rng: &mut StdRng) -> Vec<Pane> {
    let mut ret = Vec::new();
    let x_r: f32 = (w - 90.0) / 2.0;
    let y_r: f32 = (h - 90.0) / 2.0;
    let max = ((w * h) / 60.0.powi(2)) as usize;
    for _ in 0..max {
        let x = rng.gen_range(-x_r..=x_r);
        let y = rng.gen_range(-y_r..=y_r);
        let hue = rng.gen_range(240.0..300.0); // blue to magenta
        let hue = d2h(hue);
        let value = rng.gen_range(0.1..0.3);
        let sat = 1.0;
        let height = rng.gen_range(40.0..90.0);
        let center = Vector2::new(x, y);
        let p = Pane::new(height, hue, sat, value, center, rng.gen());
        ret.push(p);
    }
    ret
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.frame.to_string()
                + ".png";

            app.main_window().capture_frame(out);
        }
        Key::R => {}
        _ => {}
    }
}

fn round(x: f32) -> i32 {
    ((x + 0.5).floor() as i32).abs()
}
