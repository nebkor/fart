use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
// use rand::seq::SliceRandom;
use rand::{Rng, SeedableRng};

use std::collections::{BTreeMap, HashSet, VecDeque};
use std::default::Default as _;

const NUM_DOTS: usize = 600;
const RNG_SEED: u64 = 18;
const W_H: f32 = 700.0;

const SAFE_DIST: f32 = 35.0; // if there's a point less than this away, move away from it
const DIR_JIT: f32 = 60.0; // at each step, change direction up to +/- this many degrees
const MIN_SPD: f32 = 5.0;
const MAX_SPD: f32 = 15.0;

#[derive(Clone, Copy, Debug, Default)]
struct Dot {
    dir: f32,
    spd: f32,
    pos: Point2,
}

impl Dot {
    pub fn new(dir: f32, spd: f32, pos: Point2) -> Self {
        Dot { dir, spd, pos }
    }
}

#[derive(Debug)]
struct Model {
    dots: VecDeque<Dot>,
    by_x: BTreeMap<i32, Vec<usize>>,
    by_y: BTreeMap<i32, Vec<usize>>,
    rng: StdRng,
    rect: Rect,
    now: String,
    frame: usize,
    record: bool,
    restart: bool,
}

impl Model {
    pub fn new() -> Self {
        let mut dots = VecDeque::with_capacity(NUM_DOTS);
        let mut rng = StdRng::seed_from_u64(RNG_SEED);
        let dir = rng.gen_range(0.0..360.0);
        let dot = Dot::new(dir, MIN_SPD, Point2::default());
        dots.push_back(dot);
        Model {
            dots,
            by_x: BTreeMap::new(),
            by_y: BTreeMap::new(),
            rng,
            rect: Rect::from_w_h(W_H, W_H),
            now: format!("{}", Local::now().format("%Y%m%d_%H:%M:%S")),
            frame: 0,
            record: false,
            restart: false,
        }
    }
    pub fn now(&self) -> &str {
        &self.now
    }

    pub fn neighbors(&self, candidate: &Dot) -> Vec<usize> {
        let mut x_set: HashSet<usize> = HashSet::new();
        let mut y_set: HashSet<usize> = HashSet::new();
        let c_x = candidate.pos.x;
        let c_y = candidate.pos.y;

        let ex_len = map_range(candidate.spd, MIN_SPD, MAX_SPD, 20.0, 2.0) as usize;

        // don't count the last two inserted points as neighbors
        let mut excludes = HashSet::new();
        let last_idx = self.dots.len();
        let min_idx = last_idx.saturating_sub(ex_len);
        for ex_idx in min_idx..last_idx {
            excludes.insert(ex_idx);
        }

        let safe_dist = SAFE_DIST * (candidate.spd / MAX_SPD);

        let min_x = (c_x - safe_dist).floor() as i32;
        let max_x = (c_x + safe_dist).ceil() as i32;
        let min_y = (c_y - safe_dist).floor() as i32;
        let max_y = (c_y + safe_dist).ceil() as i32;

        for (_, v) in self.by_x.range(min_x..=max_x) {
            for i in v.iter() {
                x_set.insert(*i);
            }
        }
        for (_, v) in self.by_y.range(min_y..=max_y) {
            for i in v.iter() {
                y_set.insert(*i);
            }
        }

        x_set
            .intersection(&y_set)
            .map(|e| e.to_usize().unwrap())
            .collect::<HashSet<_>>()
            .difference(&excludes)
            .map(|e| e.to_usize().unwrap())
            .collect()
    }

    pub fn add(&mut self) {
        let idx = self.dots.len();
        let mut dot = self.dots[idx.saturating_sub(1)].clone();
        let opos = dot.pos;
        let oang = dot.dir;
        let ang = oang + self.rng.gen_range(-DIR_JIT..DIR_JIT) % 360.0;
        let dir = Vector2::from_angle(deg_to_rad(ang));
        dot.dir = ang;
        let npos = dot.pos + dir * dot.spd;
        dot.pos = npos.clone();

        let neighbors = self.neighbors(&dot);
        if neighbors.is_empty() {
            // add the coordinates book-keeping
            let x = dot.pos.x as i32;
            let y = dot.pos.y as i32;
            self.by_x.entry(x).or_insert(Vec::new()).push(idx);
            self.by_y.entry(y).or_insert(Vec::new()).push(idx);

            // update the speed and direction of the dot
            dot.spd = (dot.spd + 0.5).min(MAX_SPD);
            let cdir = dot.pos.normalize();
            // the force toward the center gets stronger as you move away form it
            let cforce = dot.pos.magnitude().powf(1.3) / (W_H * 3.5);
            let theta = dir.perp_dot(cdir).asin() * 57.2958; // degrees per radian
            let dir = dot.dir - theta * cforce;
            dot.dir = dir;
            self.dots.push_back(dot);
        } else {
            // first, iterate through previous points, constructing segments from each pair of previous points and checking for intersection
            let min = neighbors
                .clone()
                .into_iter()
                .min()
                .unwrap_or(0)
                .saturating_sub(1);
            let max = neighbors.iter().max().unwrap_or(&0) + 1;
            let max = max.min(self.dots.len());
            let neighbors: Vec<&Dot> = self.dots.range(min..max).collect();

            // now find the closest intersecting segment along our previous trajectory
            let mut min_pt: Option<Point2> = None;
            let mut min_dist = f32::MAX;
            for seg_pts in neighbors.windows(2) {
                let tp1 = seg_pts[0];
                let tp2 = seg_pts[1];
                if let Some(intersection) = intersects(&opos, &dot.pos, &tp1.pos, &tp2.pos) {
                    let npos = dot.pos - opos;
                    let ipos = intersection - opos;
                    if npos.dot(ipos) > 0.0 {
                        continue;
                    }
                    min_dist = ipos.magnitude();
                    min_pt = Some(intersection);
                }
            }
            // if there's an intersection, just move halfway from original position to the intersection
            // and adjust your direction a little to the right
            let spd = if min_pt.is_some() {
                let spd = min_dist * 0.5;
                let pos = opos + dir * spd;
                dot.dir = ang + self.rng.gen_range(5.0..15.0);
                dot.pos = pos;
                spd
            } else {
                (dot.spd * 0.5).max(MIN_SPD)
            };
            dot.spd = spd;
            let x = dot.pos.x as i32;
            let y = dot.pos.y as i32;
            self.by_x.entry(x).or_insert(Vec::new()).push(idx);
            self.by_y.entry(y).or_insert(Vec::new()).push(idx);

            self.dots.push_back(dot);
        }
    }
}

fn intersects(op1: &Point2, op2: &Point2, tp1: &Point2, tp2: &Point2) -> Option<Point2> {
    // from https://stackoverflow.com/a/19550879

    let os_x = op2.x - op1.x;
    let os_y = op2.y - op1.y;
    let ts_x = tp2.x - tp1.x;
    let ts_y = tp2.y - tp1.y;

    let denom = (os_x * ts_y) - (os_y * ts_x);

    if denom.abs() < 0.03 {
        // colinear enough
        return None;
    }

    let dip = denom > 0.0; // "denom is positive"

    let ots_x = op1.x - tp1.x;
    let ots_y = op1.y - tp1.y;

    let s_numer = os_x * ots_y - os_y * ots_x;
    if (s_numer < 0.0) == dip {
        return None;
    }

    let t_numer = ts_x * ots_y - ts_y * ots_x;
    if (t_numer < 0.0) == dip {
        return None;
    }

    if (s_numer > denom) == dip || (t_numer > denom) == dip {
        return None;
    }

    // we have collision!
    let t = t_numer / denom;
    let x = op1.x + (t * os_x);
    let y = op1.y + (t * os_y);

    Some(Point2::new(x, y))
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    app.new_window()
        .size(200 + W_H as u32, 200 + W_H as u32)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    Model::new()
}

fn update(app: &App, model: &mut Model, update: Update) {
    let len = model.dots.len();
    if len < NUM_DOTS {
        model.add();
    } else {
        model.dots.pop_front();
    }
    if model.restart {
        model.restart = false;
        model.dots.clear();
        model.by_x.clear();
        model.by_y.clear();
        let mut dot = Dot::default();
        let ang = model.rng.gen_range(0.0..360.0);
        let spd = model.rng.gen_range(MIN_SPD..MAX_SPD);
        dot.spd = spd;
        dot.dir = ang;
        model.dots.push_back(dot);
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    if frame.nth() == 0 || app.keys.down.contains(&Key::B) || app.keys.down.contains(&Key::R) {
        draw.background().color(BLACK);
    }

    draw.rect()
        .wh(app.window_rect().wh())
        .hsva(0.0, 0.1, 0.1, 0.1);

    draw.polyline().weight(10.0).join_round().points_colored(
        model
            .dots
            .iter()
            .enumerate()
            .map(|(i, d)| {
                (
                    d.pos,
                    Hsv::new(
                        map_range(i as f32, 0.0, NUM_DOTS as f32, 0.0, 250.0),
                        1.0,
                        1.0,
                    ),
                )
            })
            .collect::<Vec<(Vector2, Hsv)>>(),
    );
    draw.to_frame(app, &frame).unwrap();
    if model.record {
        let out = format!("frames/{:04}.png", frame.nth());
        app.main_window().capture_frame(out);
    }
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.frame.to_string()
                + ".png";

            app.main_window().capture_frame(out);
        }
        Key::R => {
            model.restart = true;
        }
        Key::V => {
            model.record = !model.record;
        }
        _ => {}
    }
}
