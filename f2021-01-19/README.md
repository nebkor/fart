# untitled

# Initial idea
Much like [January 3rd](../f2021-01-03)'s idea, I want to have a squiggly, space-filling line,
except I don't want the line to intersect itself. I think if it starts at the origin (which is in
the middle of the screen canvas), and it tends to want to return to that point as well as not
intersect itself, it should produce some interesting patterns. Also like f2021-01-03, if it's just a
black line on a gray background, I'm cool with that, but a color animation could also be very
cool. Could also have it grow and shrink.

I'm going to base the intersection detection on the algorithm as specified in Python in [this
stackoverflow answer](https://stackoverflow.com/a/19550879).

The idea will be that it will just be a turtle moving along with a speed and direction. Whenever it
gets within some factor X of any previous points, it finds the closest point and changes its
direction to be more along the normal from that point (by sampling next and prev and doing the
perpendicular to that).
