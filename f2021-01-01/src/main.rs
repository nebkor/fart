/**
 * Adapted from https://github.com/nannou-org/nannou/blob/0d9d18167a7007119ffa7b7e386351e592d59fda/generative_design/dynamic_data_structure/m_6_1_01.rs
 *
 * KEYS
 * r             : reset positions
 * s             : save png
 * b             : redraw background color
 */
use chrono::Utc;
use nannou::prelude::*;

fn main() {
    nannou::app(model).update(update).run();
}

struct Node {
    pub x: f32,
    pub y: f32,
    min_x: f32,
    max_x: f32,
    min_y: f32,
    max_y: f32,
    radius: f32,   // Radius of impact
    ramp: f32,     // Influences the shape of the function
    strength: f32, // Strength: positive value attracts, negative value repels
    damping: f32,
    velocity: Vector2,
    max_speed: f32,
    size: f32,
}

impl Node {
    fn new(x: f32, y: f32, min_x: f32, max_x: f32, min_y: f32, max_y: f32) -> Self {
        let mut size = 0.0;
        while size.abs() < 0.2 {
            size = random_range(-1.2, 1.0);
        }
        let max_speed = 10.0;
        let speed_boost = max_speed.sqrt();
        Node {
            x,
            y,
            min_x,
            max_x,
            min_y,
            max_y,
            radius: 200.0,
            ramp: 1.0,
            strength: size,
            damping: random_range(-0.1, 1.0),
            // random_f32() returns a float between 0 and 1, so multipy each velocity component by
            // the square root of the max_speed.
            velocity: vec2(random_f32() * speed_boost, random_f32() * speed_boost),
            max_speed,
            size,
        }
    }

    fn update(&mut self, app: &App) {
        self.velocity = self.velocity.limit_magnitude(self.max_speed);

        let speed = self.velocity.magnitude();
        let effort = speed / self.max_speed;

        let strength = self.size * effort;
        self.strength = strength;

        let win = app.main_window().rect();
        self.min_x = win.left() + 5.0;
        self.max_x = win.right() - 5.0;
        self.min_y = win.top() - 5.0;
        self.max_y = win.bottom() + 5.0;

        self.x += self.velocity.x;
        self.y += self.velocity.y;

        if self.x < self.min_x {
            self.x = self.min_x - (self.x - self.min_x);
            self.velocity.x = -self.velocity.x;
        }
        if self.x > self.max_x {
            self.x = self.max_x - (self.x - self.max_x);
            self.velocity.x = -self.velocity.x;
        }

        if self.y < self.min_y {
            self.y = self.min_y + (self.y - self.min_y);
            self.velocity.y = -self.velocity.y;
        }
        if self.y > self.max_y {
            self.y = self.max_y + (self.y - self.max_y);
            self.velocity.y = -self.velocity.y;
        }

        self.velocity *= 1.0 - self.damping;
    }
}

struct Model {
    nodes: Vec<Node>,
    node_count: usize,
    caps: usize,
    now: String,
}

fn model(app: &App) -> Model {
    app.new_window()
        .size(1400, 800)
        .view(view)
        .key_released(key_released)
        .build()
        .unwrap();

    let node_count = 1500;
    let nodes = create_nodes(node_count, app.window_rect());
    let now = Utc::now();

    Model {
        nodes,
        node_count,
        caps: 1,
        now: format!("{}", now.format("%H%M%S")),
    }
}

fn create_nodes(node_count: usize, win: Rect) -> Vec<Node> {
    (0..node_count)
        .map(|_| {
            Node::new(
                random_range(-600.0, 600.0),
                random_range(-300.0, 300.0),
                win.left() + 5.0,
                win.right() - 5.0,
                win.top() - 5.0,
                win.bottom() + 5.0,
            )
        })
        .collect()
}

fn attract_nodes(nodes: &mut Vec<Node>, target: usize) {
    for other in 0..nodes.len() {
        // Continue from the top when node is itself
        if other == target {
            continue;
        }
        let df = attract(&nodes[target], &nodes[other]);
        nodes[other].velocity += df;
    }
}

fn attract(current_node: &Node, other_node: &Node) -> Vector2 {
    let current_node_vector = vec2(current_node.x, current_node.y);
    let other_node_vector = vec2(other_node.x, other_node.y);
    let d = current_node_vector.distance(other_node_vector);

    if d > 0.0 && d < current_node.radius {
        let s = (d / current_node.radius).powf(1.0 / current_node.ramp);
        let f = s * 9.0 * current_node.strength * (1.0 / (s + 1.0) + ((s - 3.0) / 4.0)) / d;
        let mut df = current_node_vector - other_node_vector;
        df *= f;
        df
    } else {
        vec2(0.0, 0.0)
    }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    for i in 0..model.nodes.len() {
        // Let all nodes repel each other
        attract_nodes(&mut model.nodes, i);
        // Apply velocity vector and update position
        model.nodes[i].update(&app);
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    // Begin drawing
    let draw = app.draw();

    if frame.nth() == 0 || app.keys.down.contains(&Key::R) || app.keys.down.contains(&Key::B) {
        draw.background().color(BLACK);
    } else {
        draw.rect()
            .wh(app.window_rect().wh())
            .rgba(0.0, 0.0, 0.0, 0.07);
    }

    model.nodes.iter().for_each(|node| {
        let color_factor = node.size / 2.0;
        let red = 0.5 + color_factor;
        let blue = 0.5 - color_factor;
        draw.ellipse()
            .x_y(node.x, node.y)
            .radius((5.0 * (1.0 / node.velocity.magnitude())).min(5.0))
            .rgba(red, 0.2, blue, 0.7);
    });

    // Write the result of our drawing to the window's frame.
    draw.to_frame(app, &frame).unwrap();
}

fn key_released(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::R => model.nodes = create_nodes(model.node_count, app.window_rect()),
        Key::S => {
            app.main_window().capture_frame(
                app.exe_name().unwrap() + "-" + &model.now + "-" + &model.caps.to_string() + ".png",
            );
            model.caps += 1;
        }
        _other_key => {}
    }
}
