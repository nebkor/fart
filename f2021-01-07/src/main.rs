use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};

use std::thread::sleep;
use std::time::Duration;

const HEIGHT: u32 = 900;
const WIDTH: u32 = 900;
const PHI: f32 = 1.61803;
const DOT_RAD: f32 = 2.0;
const RNG_SEED: u64 = 18;

#[derive(Debug, Default, Clone)]
struct Dot {
    pub p: Point2,
    pub h: f32,
    pub s: f32,
}

#[derive(Clone, Default, Debug)]
struct Pane {
    height: f32,
    hue: f32,
    sat: f32,
    value: f32,
    center: Vector2,
    tall: bool,
}

#[derive(Debug, Clone)]
struct Orb {
    dots: Vec<Dot>,
}

impl Orb {
    pub fn new(radius: f32, rings: u32, rng: &mut StdRng) -> Self {
        let mut dots = Vec::new();
        let width = radius / (rings).max(1) as f32;
        for r in (1..=rings).rev() {
            let h = rng.gen_range(0.55..0.65);
            let r = r as f32 * width;
            let a = r.powi(2) * PI;
            let n = (3.0 * a / (DOT_RAD.powi(2) * PI)) as u32;
            for _ in 0..n {
                let s = rng.gen_range(0.0..0.1);
                let theta = rng.gen_range(0.0..=TAU);
                let r = r * rng.gen::<f32>().sqrt();
                let x = r * theta.cos();
                let y = r * theta.sin();
                let p = Point2::new(x, y);
                let d = Dot { p, h, s };
                dots.push(d);
            }
        }
        Orb { dots }
    }
}

impl Pane {
    pub fn new(height: f32, hue: f32, sat: f32, value: f32, center: Vector2, tall: bool) -> Self {
        Pane {
            height,
            hue,
            sat,
            value,
            center,
            tall,
        }
    }

    pub fn width(&self) -> f32 {
        if !self.tall {
            self.height
        } else {
            self.height / PHI
        }
    }

    pub fn height(&self) -> f32 {
        if self.tall {
            self.height
        } else {
            self.height / PHI
        }
    }

    pub fn center(&self) -> &Vector2 {
        &self.center
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    rng: StdRng,
    bg: Vec<Pane>,
    now: String,
    frame: u64,
}

impl Model {
    pub fn new() -> Self {
        let mut rng = StdRng::seed_from_u64(RNG_SEED);
        let bg = background(&mut rng);
        Model {
            rng,
            bg,
            now: format!("{}", Local::now().format("%Y_%m_%d_%H_%M_%S")),
            frame: 0,
        }
    }

    pub fn now(&self) -> &str {
        &self.now
    }
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();
    Model::new()
}

fn update(app: &App, model: &mut Model, _update: Update) {
    if model.frame % 4 == 0 {
        model.bg = background(&mut model.rng);
    }
    model.frame = app.elapsed_frames();
}
fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    if app.keys.down.contains(&Key::B) {
        draw.background().color(BLACK);
    }

    draw.rect()
        .wh(app.window_rect().wh())
        .hsva(0.0, 0.0, 0.0, 0.1);
    if frame.nth() % 12 == 0 {
        for p in model.bg.iter() {
            draw.rect()
                .height(p.height())
                .width(p.width())
                .x_y(p.center().x, p.center().y)
                .hsva(p.hue, p.sat, p.value, 0.6);
        }
    }

    let out = format!("frames/{:04}.png", frame.nth());
    app.main_window().capture_frame(out);

    draw.to_frame(app, &frame).unwrap();
}

fn background(rng: &mut StdRng) -> Vec<Pane> {
    let mut ret = Vec::new();
    let x_r: f32 = WIDTH as f32 * 0.8 / 2.0;
    let y_r: f32 = HEIGHT as f32 * 0.8 / 2.0;
    for _ in 0..1000 {
        let x = rng.gen_range(-x_r..=x_r);
        let y = rng.gen_range(-y_r..=y_r);
        let hue = rng.gen_range(0.66..0.72);
        let value = rng.gen_range(0.6..0.9);
        let sat = 1.0;
        let height = rng.gen_range(40.0..90.0);
        let center = Vector2::new(x, y);
        let p = Pane::new(height, hue, sat, value, center, rng.gen());
        ret.push(p);
    }
    ret
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.frame.to_string()
                + ".png";

            app.main_window().capture_frame(out);
        }
        Key::R => {}
        _ => {}
    }
}
