# No title, voronoi tessalation with orange and blue

## Initial idea
Based on the code from [f2021-01-11](../f2021-01-11), this adds a tendancy to turn left to the birbs
and then uses their positions as points in a [Voronoi
tessalation](https://en.wikipedia.org/wiki/Voronoi_diagram). I was going for a spiral or swirl
motion; Kris suggested the color pallete.

## Result
Once I got the voronoi crate working (it expects all coordinates to be strictly positive), it pretty
much Just Worked the way I was hoping it would. Success!

![nice](./blorange_veranoy.png)
