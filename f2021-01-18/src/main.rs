use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
use rand::seq::SliceRandom;
use rand::{Rng, SeedableRng};
use voronoi::{make_polygons, voronoi, Point};

const PHI: f32 = 1.61;

const WIDTH: u32 = 1400;
const HEIGHT: u32 = 1400;

// birb dimensions
const BHEIGHT: f32 = 10.0;
const HVAR: f32 = BHEIGHT / 2.0;
const BWIDTH: f32 = BHEIGHT / PHI;
const WVAR: f32 = BWIDTH / 2.0;
const DIST: f32 = 2.0 * BWIDTH;
const DJIT: f32 = 0.2 * BWIDTH;
const MIN_SPEED: f32 = 2.0;
const MIN_JIT: f32 = 0.5 * MIN_SPEED;
const MAX_SPEED: f32 = 3.0 * MIN_SPEED;
const MAX_JIT: f32 = 0.5 * MAX_SPEED;
const BOOST: f32 = 1.1;
const BJIT: f32 = BOOST * 0.3;

const BIRBS: usize = 200;

const RNG_SEED: u64 = 18;

#[derive(Clone, Default, Debug, Copy)]
struct Birb {
    height: f32,
    width: f32,
    hue: f32,
    sat: f32,
    value: f32,
    center: Vector2,
    dir: Vector2,
    spd: f32,
    min_s: f32, // min speed
    max_s: f32, // max speed
    min_d: f32, // minimum distance, as in, "nothing closer than this"
    boost: f32, // how horny is this birb
}

trait Vable {
    fn to_vector(&self) -> Vector2;
}

impl Vable for Point {
    fn to_vector(&self) -> Vector2 {
        Vector2::new(self.x() as f32, self.y() as f32)
    }
}

impl Birb {
    pub fn new(
        height: f32,
        width: f32,
        hue: f32,
        sat: f32,
        value: f32,
        center: Vector2,
        dir: Vector2,
        spd: f32,
        min_s: f32,
        max_s: f32,
        min_d: f32,
        boost: f32,
    ) -> Self {
        Birb {
            height,
            width,
            hue,
            sat,
            value,
            center,
            dir,
            spd,
            min_s,
            max_s,
            min_d,
            boost,
        }
    }

    pub fn attract(&self, other: &Self) -> Vector2 {
        let mut cdiff = (self.hue - other.hue).abs(); // 0..360
        if cdiff > 180.0 {
            cdiff = 360.0 - cdiff;
        }
        let v_d = other.center - self.center;
        let d = v_d.magnitude();
        let d = 20.0 * (1.0 / d.powf(1.7)) * self.boost;
        let strength = map_range(cdiff, 0.0, 180.0, -1.0, 1.0) * d;
        (other.center - self.center).normalize() * strength
    }

    pub fn points(&self) -> Vec<Point2> {
        let h_2 = self.height / 2.0;
        let w_2 = self.width / 2.0;
        let mut ret = Vec::with_capacity(3);
        let dir = self.dir;
        let top = self.center + (dir * h_2);
        let bottom = self.center - (dir * h_2);
        let right = perp_right(bottom) * w_2;
        let left = right * -1.0;
        ret.push(right + bottom);
        ret.push(left + bottom);
        ret.push(top);
        ret
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    rng: StdRng,
    birbs: Vec<Birb>,
    now: String,
    nth_frame: u64,
    record: bool,
    restart: bool,
}

impl Model {
    pub fn new() -> Self {
        let mut rng = StdRng::seed_from_u64(RNG_SEED);
        let birbs = mk_birbs(&mut rng);
        Model {
            rng,
            birbs,
            now: format!("{}", Local::now().format("%Y%m%d_%H:%M:%S")),
            nth_frame: 0,
            record: false,
            restart: false,
        }
    }

    pub fn now(&self) -> &str {
        &self.now
    }

    pub fn update_birbs(&mut self, wh: Vector2) {
        let max_x = wh[0] / 2.0;
        let max_y = wh[1] / 2.0;
        let min_x = -max_x;
        let min_y = -max_y;
        for i in 0..self.birbs.len() {
            let mut d = f32::MAX;
            let mut closest_o = Vector2::default();
            let mut b = self.birbs[i].clone();
            let mut v = b.dir * b.spd;

            for o in 0..self.birbs.len() {
                let other = &self.birbs[o];
                if o == i {
                    continue;
                }
                let v_o = other.center - b.center;
                let d_o = v_o.magnitude();
                if d_o < d {
                    d = d_o;
                    closest_o = v_o;
                }
                v += b.attract(&other);
            }
            let spd = v.magnitude().max(b.min_s).min(b.max_s);
            if d < b.min_d {
                v -= closest_o.normalize() * spd * 0.1;
            }

            // turn toward the center
            let turn = Vector2::from_angle(v.angle() - deg_to_rad(3.0)) * spd;
            v += turn;
            b.dir = v.normalize();
            b.spd = spd * 1.2;

            self.birbs[i] = b;
        }
        for i in 0..self.birbs.len() {
            let mut b = self.birbs[i].clone();
            b.center = b.center + (b.dir * b.spd);
            if b.center.x < (min_x + b.height) {
                b.dir.x *= -1.0;
                b.center.x = min_x + b.height * 2.0;
            } else if b.center.x > (max_x - b.height) {
                b.dir.x *= -1.0;
                b.center.x = max_x - b.height * 2.0;
            }
            if b.center.y < (min_y + b.height) {
                b.dir.y *= -1.0;
                b.center.y = min_y + b.height * 2.0;
            } else if b.center.y > (max_y - b.height) {
                b.dir.y *= -1.0;
                b.center.y = max_y - b.height * 2.0;
            }
            self.birbs[i] = b;
        }
    }
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();
    Model::new()
}

fn update(app: &App, model: &mut Model, _update: Update) {
    model.update_birbs(app.main_window().rect().wh());
    model.nth_frame = app.elapsed_frames();
    if model.restart {
        model.birbs = mk_birbs(&mut model.rng);
        model.restart = false;
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    // blank everything out, start visually afresh
    if app.keys.down.contains(&Key::B) || model.restart || app.keys.down.contains(&Key::R) {
        draw.background().color(BLACK);
    }

    // background, paint over previous birbs with transparent mauve (but no v, so actually black)
    draw.rect()
        .wh(app.window_rect().wh())
        .hsva(d2h(300.0), 0.17, 0.00, 0.08);

    let rect = app.window_rect();
    let size = rect.w().max(rect.h());
    let comp = Vector2::new(rect.left(), rect.bottom());

    let vernoy = voronoi(
        model
            .birbs
            .iter()
            .map(|b| {
                let p = b.center - comp;
                Point::new(p.x as f64, p.y as f64)
            })
            .collect::<Vec<Point>>(),
        size as f64,
    );
    let polys = make_polygons(&vernoy);

    for i in 0..(polys.len().min(model.birbs.len())) {
        let t = model.birbs[i];
        let hue = d2h(t.hue);
        let sat = t.sat;
        let val = t.value;

        let p = &polys[i];
        draw.polygon()
            .stroke(WHITESMOKE)
            .stroke_weight(4.0)
            .points(
                p.iter()
                    .map(|p| p.to_vector() + comp)
                    .collect::<Vec<Vector2>>(),
            )
            .hsva(hue, sat, val, 1.0);
        // draw.ellipse()
        //     .xy(t.center)
        //     .radius(t.width)
        //     .hsva(hue, sat, val, 0.8);
    }

    draw.to_frame(app, &frame).unwrap();
    if model.record {
        let out = format!("frames/{:04}.png", frame.nth());
        app.main_window().capture_frame(out);
    }
}

fn d2h(d: f32) -> f32 {
    let d = d % 360.0;
    d / 360.0
}

fn perp_right(v: Vector2) -> Vector2 {
    let theta = v.angle() + deg_to_rad(90.0);
    Vector2::from_angle(theta)
}

fn mk_birbs(rng: &mut StdRng) -> Vec<Birb> {
    let mut birbs = Vec::new();
    for _ in 0..BIRBS {
        let r = rng.gen_range(0.0..20.0);
        let theta = rng.gen_range(0.0..TAU);
        let center = Vector2::new(theta * r.cos(), theta * r.sin());

        let theta = rng.gen_range(0.0..TAU);
        let dir = Vector2::from_angle(theta);

        let min_s = MIN_SPEED + rng.gen_range(-MIN_JIT..MIN_JIT);
        let max_s = MAX_SPEED + rng.gen_range(-MAX_JIT..MAX_JIT);
        let spd = rng.gen_range(min_s..max_s);
        let min_d = DIST + rng.gen_range(-DJIT..DJIT);

        let hue: f32 = *[rng.gen_range(230.0..250.0), rng.gen_range(35.0..55.0)]
            .choose(rng)
            .unwrap();

        let value = rng.gen_range(0.6..0.9);
        let height = BHEIGHT + rng.gen_range(-HVAR..HVAR);
        let sat = height / BHEIGHT;
        let width = BWIDTH + rng.gen_range(-WVAR..WVAR);
        let boost = BOOST + rng.gen_range(-BJIT..BJIT);
        let c = Birb::new(
            height, width, hue, sat, value, center, dir, spd, min_s, max_s, min_d, boost,
        );
        birbs.push(c);
    }
    birbs
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.nth_frame.to_string()
                + ".png";

            app.main_window().capture_frame(out);
        }
        Key::R => {
            model.restart = true;
        }
        Key::V => {
            model.record = !model.record;
        }
        _ => {}
    }
}
