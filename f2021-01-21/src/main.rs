use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};

const WIDTH: u32 = 1000;

const RNG_SEED: u64 = 18;

const COLOR_BASE: f32 = 240.0; // degrees, blue
const COLOR_JITTER: f32 = 15.0; // +/- 15 degrees on the color wheel

const VAL_JITTER: f32 = 0.2;
const VAL_BASE: f32 = 1.0 - VAL_JITTER;

const SAT_JITTER: f32 = 0.3;
const SAT_BASE: f32 = 1.0 - SAT_JITTER;

const PSIZE: f32 = 200.0;

use std::default::Default;

#[derive(Clone, Default, Debug)]
struct Pane {
    height: f32,
    hue: f32,
    sat: f32,
    value: f32,
    center: Vector2,
}

#[derive(Default, Clone)]
struct Quardrant {
    center: Point2,
    panes: Vec<Pane>,
}

impl Quardrant {
    pub fn new(center: Point2, hue: f32, rng: &mut impl Rng) -> Self {
        let mut panes: Vec<Pane> = Vec::new();
        let mut psize = PSIZE;
        while psize > PSIZE * 0.05 {
            let sat = SAT_BASE + rng.gen_range(-SAT_JITTER..SAT_JITTER);
            let val = VAL_BASE + rng.gen_range(-VAL_JITTER..VAL_JITTER);
            let p = Pane::new(psize, hue, sat, val, -center);
            panes.push(p);
            psize -= rng.gen_range(11.0..19.0);
        }

        Quardrant { center, panes }
    }
}

impl Pane {
    pub fn new(height: f32, hue: f32, sat: f32, value: f32, center: Vector2) -> Self {
        Pane {
            height,
            hue,
            sat,
            value,
            center,
        }
    }

    pub fn width(&self) -> f32 {
        self.height
    }

    pub fn height(&self) -> f32 {
        self.height
    }

    pub fn center(&self) -> &Vector2 {
        &self.center
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    rng: StdRng,
    quads: Vec<Quardrant>,
    now: String,
    frame: u64,
    record: bool,
}

fn mk_hue(rng: &mut impl Rng) -> f32 {
    let h = COLOR_BASE + rng.gen_range(-COLOR_JITTER..COLOR_JITTER);
    h / 360.0
}

impl Model {
    pub fn new() -> Self {
        let mut rng = StdRng::seed_from_u64(RNG_SEED);
        let mut quads = Vec::new();
        for i in 0..4 {
            let hue = rng.gen();
            let ang = 45.0 + (90.0 * i as f32);
            dbg!(ang);
            let ang = deg_to_rad(ang);
            let rad = (2.0 * (PSIZE / 2.0).powi(2)).sqrt();
            let center = Point2::from_angle(ang) * rad;
            dbg!(center);
            let quad = Quardrant::new(center, hue, &mut rng);
            quads.push(quad);
        }

        Model {
            rng,
            quads,
            now: format!("{}", Local::now().format("%Y%m%d_%H:%M:%S")),
            frame: 0,
            record: false,
        }
    }

    pub fn now(&self) -> &str {
        &self.now
    }
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(WIDTH, WIDTH)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();
    Model::new()
}

fn update(app: &App, model: &mut Model, _update: Update) {
    model.frame = app.elapsed_frames();
    let t = (model.frame % 360) as f32;
    let rad = 2.0.sqrt() * PSIZE / 2.0;
    for (i, q) in model.quads.iter_mut().enumerate() {
        let dir = match i {
            0 => -1.0,
            1 => 1.0,
            2 => -1.0,
            3 => 1.0,
            _ => 0.0,
        };
        let deg_base = 45.0 + (i as f32 * 90.0);
        let degs = deg_base + t;
        let ang = deg_to_rad(degs) * dir;
        dbg!(i, ang);
        let center = Point2::from_angle(ang) * -rad;
        let center = q.center + center;
        dbg!(q.center, center);
        let hue = model.rng.gen();
        for p in q.panes.iter_mut() {
            if t.abs() == 180.0 && false {
                p.hue = hue;
                p.sat = SAT_BASE + model.rng.gen_range(-SAT_JITTER..SAT_JITTER);
                p.value = VAL_BASE + model.rng.gen_range(-VAL_JITTER..VAL_JITTER);
            }
            p.center.x = center.x;
            p.center.y = center.y;
        }
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();
    let fnum = frame.nth();

    if fnum == 0 || app.keys.down.contains(&Key::R) || app.keys.down.contains(&Key::B) {
        draw.background().color(BLACK);
    }

    let rect = app.main_window().rect();

    draw.rect().wh(rect.wh()).hsva(d2h(25.0), 0.1, 0.01, 0.02);

    for (i, q) in model.quads.iter().enumerate() {
        let mut center = Point2::default();
        for pane in q.panes.iter() {
            draw.rect()
                .height(pane.height())
                .width(pane.width())
                .xy(*pane.center())
                .hsva(pane.hue, pane.sat, pane.value, 0.7);
            center = *pane.center();
        }
        draw.text(&format!("{}", i))
            .font_size(35)
            .xy(center)
            .color(BLACK)
            .height(40.0);
    }
    draw.to_frame(app, &frame).unwrap();

    if model.record {
        let out = format!("frames-{}/{:04}.png", model.now(), frame.nth());
        app.main_window().capture_frame(out);
    }
}

// this is maybe "degress to turns", though even then it's resetting at 360, so idunno
fn d2h(d: f32) -> f32 {
    (d % 360.0) / 360.0
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.frame.to_string()
                + ".png";
            app.main_window().capture_frame(out);
        }
        Key::V => {
            model.record = !model.record;
        }
        _ => {}
    }
}
