use nannou::color::Hsva;
use nannou::geom::Point2;

pub trait Smells {
    fn base_name(&self) -> String {
        unimplemented!()
    }
}

pub fn capture_name<T: Smells>(base: &str, fart: &T) -> String {
    format!("{}-{}.png", base, fart.base_name())
}

#[derive(Debug)]
pub enum GOrient {
    // The usize is the number of steps in the gradient
    // It doesn't have to be very many; the gpu is going to interpolate smoothly between them
    Vertical(usize),
    Horizontal(usize),
}

pub struct GRect {
    w: f32,
    h: f32,
    center: Point2,
    orient: GOrient,
}

impl GRect {
    pub fn new(w: f32, h: f32, center: Point2, orient: GOrient) -> Self {
        GRect {
            w,
            h,
            center,
            orient,
        }
    }

    pub fn points_colored<F>(&self, color_fn: F) -> Vec<(Point2, Hsva)>
    where
        F: Fn(f32, f32, f32) -> Hsva,
    {
        let w_2 = self.w / 2.0;
        let h_2 = self.h / 2.0;
        let x_max = self.center.x + w_2;
        let x_min = self.center.x - w_2;
        let y_max = self.center.y + h_2;
        let y_min = self.center.y - h_2;
        let mut points: Vec<(Point2, Hsva)>;
        match self.orient {
            GOrient::Vertical(steps) => {
                let steps = steps.max(2);
                points = Vec::with_capacity(2 * steps);
                let dy = self.h / (steps - 1) as f32;
                let mut y = y_min;
                for i in 0..steps {
                    y = y_min + (i as f32 * dy);
                    y = y.min(y_max);
                    let c = color_fn(y, y_min, y_max);
                    let p = Point2::new(x_min, y);
                    points.push((p, c));
                }
                for i in 0..steps {
                    y = y_max - (i as f32 * dy);
                    y = y.max(y_min);
                    let c = color_fn(y, y_min, y_max);
                    let p = Point2::new(x_max, y);
                    points.push((p, c));
                }
            }
            GOrient::Horizontal(steps) => {
                let steps = steps.max(2);
                points = Vec::with_capacity(2 * steps);
                let dx = self.w / steps as f32;
                let mut x = x_min;
                for i in 0..steps {
                    x = x_min + (i as f32 * dx);
                    x = x.min(x_max);
                    let c = color_fn(x, x_min, x_max);
                    let p = Point2::new(x, y_min);
                    points.push((p, c));
                }
                for i in (0..steps).rev() {
                    x = x_min + (i as f32 * dx);
                    x = x.max(x_min);
                    let c = color_fn(x, x_min, x_max);
                    let p = Point2::new(x, y_max);
                    points.push((p, c));
                }
            }
        }
        points
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
