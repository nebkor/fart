# Downtown, or, Petula Clark's Revenge

Originally, I wanted a shifting, irregular grid of black and white polygons, all with only right
angles. I wanted one of them to be red, and that red stain wander down the grid, from top to
bottom, leaving a trail as it went.

To really get what I wanted initially, I'll need to do a Voronoi tiling using Manhattan distance,
and modify it so that the 45-degree lines get turned into complementary 90-degree ones. I have never
seen this in the wild, so it'll have to be from scratch.

In the meantime, I got a similar effect using a space-partitioning structure called an "R-tree",
that makes bounding boxes for points inside it and is normally used for doing like nearest-neighbor
querying.

![a midcentury modern nightmare](./downtown-20210221_18:44:24-seed-18-885.png)
