use chrono::Local;
use nannou::prelude::*;
use rand::distributions::Uniform;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};
use rstar::{Point, PointDistance, RStarInsertionStrategy, RTree, RTreeNode, RTreeParams, AABB};

use std::collections::VecDeque;

const NUM_POINTS: usize = 15_000;
const BATCH_SIZE: usize = 50;
const RNG_SEED: u64 = 18;
const PHI: f32 = 1.6108;
const HEIGHT: f32 = 2048.0;
const WIDTH: f32 = HEIGHT / PHI;
const MAX_Y: f32 = HEIGHT / 2.0;
const MAX_X: f32 = WIDTH / 2.0;

mod two_d;

#[derive(Default, Debug)]
struct Stain {
    center: TreePointType2D,
    w: f32,
    h: f32,
}

impl Stain {
    fn new(center: TreePointType2D, rng: &mut impl Rng) -> Self {
        Stain {
            center,
            w: rng.gen_range(5.0..300.0),
            h: rng.gen_range(5.0..300.0),
        }
    }
}

pub struct Model {
    points: VecDeque<TreePointType2D>,
    tree_2d: DemoTree2D,
    red: Stain,
    frame: u64,
    now: String,
    rng: StdRng,
    reset: bool,
}

impl Model {
    fn new() -> Self {
        let rng = StdRng::seed_from_u64(RNG_SEED);
        let mut model = Model {
            points: VecDeque::with_capacity(NUM_POINTS),
            tree_2d: DemoTree2D::default(),
            red: Stain::default(),
            frame: 0,
            now: format!("{}", Local::now().format("%Y%m%d_%H:%M:%S")),
            rng,
            reset: true,
        };
        let mut len = 0;
        while len < NUM_POINTS {
            model.insert_batch(WIDTH, HEIGHT);
            len += BATCH_SIZE;
        }
        model.update_red();
        model
    }

    fn insert_batch(&mut self, window_width: f32, window_height: f32) {
        let mut points_2d = create_random_points::<TreePointType2D>(BATCH_SIZE, &mut self.rng);
        for &mut [ref mut x, ref mut y] in &mut points_2d {
            *x = *x * window_width * 0.5;
            *y = *y * window_height * 0.5;
        }
        for p in points_2d.iter() {
            self.tree_2d.insert(*p);
            self.points.push_back(*p);
        }
    }

    fn remove_batch(&mut self) {
        for _ in 0..BATCH_SIZE {
            if let Some(p) = self.points.pop_front() {
                let _ = self.tree_2d.remove(&p);
            }
        }
    }

    fn update_red(&mut self) {
        // real quick just handle the reset case
        let center = [self.rng.gen_range(-MAX_X..MAX_X), MAX_Y];
        if self.reset {
            self.red = Stain::new(center, &mut self.rng);
            self.reset = false;
            return;
        }

        let points: Vec<_> = self
            .tree_2d
            .locate_within_distance(self.red.center, 10_000.0)
            .filter(|p| p.nth(1) < self.red.center.nth(1))
            .collect();
        let mut min_d = f32::MAX;
        // re-use the previous center, it'll be correct for the case that there are on points below
        let mut center = center;
        for p in points {
            let d = self.red.center.distance_2(p);
            if d < min_d {
                min_d = d;
                center = *p;
            }
        }
        self.red = Stain::new(center, &mut self.rng);
    }
}

type DemoTree2D = RTree<TreePointType2D>;
type TreePointType2D = [f32; 2];

pub struct Params;
impl RTreeParams for Params {
    const MIN_SIZE: usize = 1;
    const MAX_SIZE: usize = 2;
    const REINSERTION_COUNT: usize = 1;
    type DefaultInsertionStrategy = RStarInsertionStrategy;
}

type RenderData = Vec<two_d::LineRenderData2D>;

fn main() {
    nannou::app(model).update(update).run();
}

fn update(app: &App, model: &mut Model, _update: Update) {
    model.frame = app.main_window().elapsed_frames();
    model.remove_batch();
    model.insert_batch(WIDTH, HEIGHT);
    if model.rng.gen::<f32>() < 0.3 {
        model.update_red();
    }
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(WIDTH as u32, HEIGHT as u32)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();

    Model::new()
}

fn view(app: &App, model: &Model, frame: Frame) {
    let render_data = create_render_data_from_model(&model);
    let draw = app.draw();

    if frame.nth() < 1 || app.keys.down.contains(&Key::R) {
        draw.background().color(WHITE);
    }

    draw.rect()
        .wh(app.main_window().rect().wh())
        .hsva(0.0, 0.0, 1.0, 0.02);
    draw_blood(&draw, &model);
    draw_tree(&draw, &render_data);

    draw.to_frame(app, &frame).unwrap();
}

pub fn create_random_points<P: Point<Scalar = f32>>(num_points: usize, rng: &mut StdRng) -> Vec<P> {
    let mut result = Vec::with_capacity(num_points);
    let distribution = Uniform::new(-1.0f32, 1.0);
    for _ in 0..num_points {
        let points: [f32; 2] = [rng.sample(distribution), rng.sample(distribution)];
        let new_point = P::generate(|i| points[i]);
        result.push(new_point);
    }
    result
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now.as_str()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.frame.to_string()
                + ".png";
            app.main_window().capture_frame(out);
        }
        Key::R => {
            model.reset = true;
        }
        _ => {}
    }
}

fn draw_blood(draw: &Draw, model: &Model) {
    draw.rect()
        .xy(model.red.center.into())
        .w(model.red.w)
        .h(model.red.h)
        .hsva(0.997, 1.0, 0.7, 0.3);
}

fn draw_tree(draw: &Draw, render_data: &[two_d::LineRenderData2D]) {
    for (from, to, level) in render_data {
        if *level == 5 {
            draw.line()
                .weight(5.0)
                .start(*from)
                .end(*to)
                .hsva(0.0, 1.0, 0.0, 0.7);
        }
    }
}

fn create_render_data_from_model(model: &Model) -> RenderData {
    crate::two_d::create_render_data_for_tree_2d(&model.tree_2d)
}
