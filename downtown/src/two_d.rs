use crate::*;

pub type LineRenderData2D = (Point2<f32>, Point2<f32>, u64);

pub fn create_render_data_for_tree_2d(tree: &DemoTree2D) -> RenderData {
    let mut lines = Vec::new();
    let mut to_visit = vec![(tree.root(), 0)];
    while let Some((cur, depth)) = to_visit.pop() {
        push_rectangle(&mut lines, depth, &cur.envelope());
        for child in cur.children() {
            if let RTreeNode::Parent(ref data) = child {
                to_visit.push((data, depth + 1));
            }
        }
    }
    lines
}

fn push_rectangle(
    result: &mut Vec<LineRenderData2D>,
    depth: u64,
    envelope: &AABB<TreePointType2D>,
) {
    let c00: Point2<_> = envelope.lower().into();
    let c11: Point2<_> = envelope.upper().into();
    let c01 = Point2::new(c00[0], c11[1]);
    let c10 = Point2::new(c11[0], c00[1]);
    result.push((c00, c01, depth));
    result.push((c01, c11, depth));
    result.push((c11, c10, depth));
    result.push((c10, c00, depth));
}
