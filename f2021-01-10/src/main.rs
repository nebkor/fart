use chrono::Local;
use nannou::prelude::*;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};

const PHI: f32 = 1.6; // lies

const WIDTH: u32 = 1242;
const HEIGHT: u32 = 2000;

const THEIGHT: f32 = 100.0;
const TWIDTH: f32 = THEIGHT / PHI;
const TROT: f32 = 2.0;
const NEST: usize = 5;

const FWIDTH: f32 = 80.0;

const RNG_SEED: u64 = 18;

#[derive(Debug, Default, Clone)]
struct PFrame {
    points: Vec<Vec<Point2>>,
    rot: f32,
}

impl PFrame {
    pub fn new(points: Vec<Vec<Point2>>) -> Self {
        PFrame { points, rot: 0.0 }
    }
}

#[derive(Clone, Default, Debug)]
struct Tryangle {
    height: f32,
    width: f32,
    rot: f32,
    hue: f32,
    sat: f32,
    value: f32,
    center: Vector2,
    cjit: Vector2,
    up: bool,
}

impl Tryangle {
    pub fn new(
        height: f32,
        width: f32,
        rot: f32,
        hue: f32,
        sat: f32,
        value: f32,
        center: Vector2,
        up: bool,
    ) -> Self {
        Tryangle {
            height,
            width,
            rot,
            hue,
            sat,
            value,
            center,
            cjit: Vector2::default(),
            up,
        }
    }

    pub fn width(&self) -> f32 {
        self.width
    }

    pub fn height(&self) -> f32 {
        self.height
    }

    pub fn points(&self) -> Vec<Point2> {
        let up = if self.up { 1.0 } else { -1.0 };
        let h_2 = up * self.height / 2.0;
        let w_2 = self.width / 2.0;
        let mut ret = Vec::with_capacity(3);
        ret.push(self.center + Vector2::new(0.0, h_2));
        ret.push(self.center + Vector2::new(-w_2, -h_2));
        ret.push(self.center + Vector2::new(w_2, -h_2));
        ret
    }

    pub fn newjit(&mut self, rng: &mut StdRng) {
        let xr = self.width() / 20.0;
        let yr = self.height() / 4.0;
        self.cjit.x = rng.gen_range(-xr..xr);
        self.cjit.y = rng.gen_range(-yr..yr);
    }
}

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    rng: StdRng,
    trys: Vec<Tryangle>,
    frame: PFrame,
    now: String,
    nth_frame: u64,
    record: bool,
}

impl Model {
    pub fn new() -> Self {
        let mut rng = StdRng::seed_from_u64(RNG_SEED);
        let trys = mk_trys(&mut rng);
        let frame = mk_frame(&mut rng);
        Model {
            rng,
            trys,
            frame,
            now: format!("{}", Local::now().format("%Y_%m_%d_%H_%M_%S")),
            nth_frame: 0,
            record: false,
        }
    }

    pub fn now(&self) -> &str {
        &self.now
    }
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .key_pressed(key_pressed)
        .build()
        .unwrap();
    Model::new()
}

fn update(app: &App, model: &mut Model, _update: Update) {
    if model.nth_frame % 60 == 0 {
        model.frame = mk_frame(&mut model.rng);
    }
    if model.nth_frame % 15 == 0 {
        model.frame.rot = model.rng.gen_range(-0.7..0.7);
        for t in model.trys.iter_mut() {
            t.rot = model.rng.gen_range(-TROT..TROT);
            t.newjit(&mut model.rng);
        }
    }

    if model.nth_frame % 30 == 0 {
        model.trys = mk_trys(&mut model.rng);
    }

    model.nth_frame = app.elapsed_frames();
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    if app.keys.down.contains(&Key::B) {
        draw.background().color(BLACK);
    }

    draw.rect()
        .wh(app.window_rect().wh())
        .hsva(d2h(300.0), 0.2, 0.15, 0.15);

    let f_hue = d2h(24.0);
    let rot = model.frame.rot;
    for p in model.frame.points.iter() {
        draw.quad()
            .points(p[0], p[1], p[2], p[3])
            .rotate(deg_to_rad(rot))
            .hsva(f_hue, 0.85, 0.2, 0.4);
    }

    for t in model.trys.iter() {
        let points = t.points();
        let j = t.cjit;
        let rot = deg_to_rad(t.rot);
        let hue = t.hue;
        let sat = t.sat;
        let val = t.value;
        draw.tri()
            .points(points[0] + j, points[1] + j, points[2] + j)
            .rotate(rot)
            .hsva(hue, sat, val, 0.1);
    }

    draw.to_frame(app, &frame).unwrap();
    if model.record {
        let out = format!("frames/{:04}.png", frame.nth());
        app.main_window().capture_frame(out);
    }
}

fn d2h(d: f32) -> f32 {
    d / 360.0
}

fn grid(rng: &mut StdRng) -> Vec<Tryangle> {
    let x_range = WIDTH as f32 - FWIDTH * 2.0;
    let y_range = HEIGHT as f32 - FWIDTH * 2.0;
    let dx = TWIDTH / 2.0;
    let dy = THEIGHT / 1.8;

    let cols = (x_range / dx).floor() as usize - 3;
    let rows = (y_range / dy).floor() as usize - 3;
    let mut ret = Vec::with_capacity(rows * cols / 2);

    let x_min = -x_range / 2.0;
    let y_min = -y_range / 2.0;

    for r in 0..rows {
        let y = dy * 2.0 + (r as f32 * dy * 0.99) + y_min + 10.0;
        for c in 0..cols {
            let x = 2.0 * dx + (c as f32 * dx) + x_min + 10.0;
            // columns and rows have the same parity
            let do_point = (c % 2 == 0 && r % 2 == 0) || (r % 2 != 0 && c % 2 != 0);
            if do_point {
                let hue = rng.gen_range(-60.0..30.0); // pink to green
                let hue = d2h(hue);
                let value = rng.gen_range(0.5..0.8);
                let sat = 1.0;
                let rot = rng.gen_range(-TROT..TROT);
                let t = Tryangle::new(
                    THEIGHT,
                    TWIDTH,
                    rot,
                    hue,
                    sat,
                    value,
                    Vector2::new(x, y),
                    r % 2 == 0,
                );
                ret.push(t);
            }
        }
    }

    ret
}

fn mk_trys(rng: &mut StdRng) -> Vec<Tryangle> {
    let mut ret = grid(rng);
    let mut fills: Vec<Tryangle> = Vec::with_capacity(ret.len() * (NEST - 1));

    for t in ret.iter() {
        for i in 1..NEST {
            let hue = rng.gen_range(-30.0..150.0); // pink to green
            let hue = d2h(hue);
            let value = rng.gen_range(0.7..1.0);

            let height = THEIGHT * 0.9.powi(i as i32);
            let sat = height / THEIGHT;
            let width = TWIDTH * 0.9.powi(i as i32);
            let rot = rng.gen_range(-1.5..1.5);
            let center = t.center.clone();
            let c = Tryangle::new(height, width, rot, hue, sat, value, center, t.up);
            fills.push(c);
        }
    }
    ret.append(&mut fills);
    ret
}

fn mk_frame(rng: &mut StdRng) -> PFrame {
    let mut frame = Vec::with_capacity(4);

    let x_max = WIDTH as f32 / 2.0 - 50.0;
    let y_max = HEIGHT as f32 / 2.0 - 60.0;

    let xjit = -7.0..7.0;
    let yjit = -10.0..10.0;

    // top
    {
        let mut top = Vec::with_capacity(4);
        // left points
        let x = -x_max + rng.gen_range(xjit.clone());
        let y = y_max + rng.gen_range(yjit.clone());
        top.push(Vector2::new(x, y));
        let x = -x_max + rng.gen_range(xjit.clone());
        let y = y_max - FWIDTH + rng.gen_range(yjit.clone());
        top.push(Vector2::new(x, y));

        // right points
        let x = x_max + rng.gen_range(xjit.clone());
        let y = y_max - FWIDTH + rng.gen_range(yjit.clone());
        top.push(Vector2::new(x, y));
        let x = x_max + rng.gen_range(xjit.clone());
        let y = y_max + rng.gen_range(yjit.clone());
        top.push(Vector2::new(x, y));

        frame.push(top);
    }

    // right
    {
        let mut right = Vec::with_capacity(4);
        // top points
        let x = x_max - FWIDTH + rng.gen_range(xjit.clone());
        let y = y_max + rng.gen_range(yjit.clone());
        right.push(Vector2::new(x, y));
        let x = x_max + rng.gen_range(xjit.clone());
        let y = y_max + rng.gen_range(yjit.clone());
        right.push(Vector2::new(x, y));

        // bottom points
        let x = x_max + rng.gen_range(xjit.clone());
        let y = -y_max + rng.gen_range(yjit.clone());
        right.push(Vector2::new(x, y));
        let x = x_max - FWIDTH + rng.gen_range(xjit.clone());
        let y = -y_max + rng.gen_range(yjit.clone());
        right.push(Vector2::new(x, y));
        frame.push(right);
    }

    // bottom
    {
        let mut bottom = Vec::with_capacity(4);
        // left points
        let x = -x_max + rng.gen_range(xjit.clone());
        let y = -y_max + rng.gen_range(yjit.clone());
        bottom.push(Vector2::new(x, y));
        let x = -x_max + rng.gen_range(xjit.clone());
        let y = -y_max + FWIDTH + rng.gen_range(yjit.clone());
        bottom.push(Vector2::new(x, y));

        // right points
        let x = x_max + rng.gen_range(xjit.clone());
        let y = -y_max + FWIDTH + rng.gen_range(yjit.clone());
        bottom.push(Vector2::new(x, y));
        let x = x_max + rng.gen_range(xjit.clone());
        let y = -y_max + rng.gen_range(yjit.clone());
        bottom.push(Vector2::new(x, y));
        frame.push(bottom);
    }

    // left
    {
        let mut left = Vec::with_capacity(4);
        // top points
        let x = -x_max + rng.gen_range(xjit.clone());
        let y = y_max + rng.gen_range(yjit.clone());
        left.push(Vector2::new(x, y));
        let x = -x_max + FWIDTH + rng.gen_range(xjit.clone());
        let y = y_max + rng.gen_range(yjit.clone());
        left.push(Vector2::new(x, y));

        // bottom points
        let x = -x_max + FWIDTH + rng.gen_range(xjit.clone());
        let y = -y_max + rng.gen_range(yjit.clone());
        left.push(Vector2::new(x, y));
        let x = -x_max + rng.gen_range(xjit.clone());
        let y = -y_max + rng.gen_range(yjit.clone());
        left.push(Vector2::new(x, y));
        frame.push(left);
    }
    PFrame::new(frame)
}

fn key_pressed(app: &App, model: &mut Model, key: Key) {
    match key {
        Key::S => {
            let out = app.exe_name().unwrap()
                + "-"
                + model.now()
                + "-seed-"
                + &RNG_SEED.to_string()
                + "-"
                + &model.nth_frame.to_string()
                + ".png";

            app.main_window().capture_frame(out);
        }
        Key::R => {
            model.record = !model.record;
        }
        _ => {}
    }
}

fn round(x: f32) -> i32 {
    ((x + 0.5).floor() as i32).abs()
}
