# *Colorist Birds*

## Initial idea
A flock of birds with different colors. They have different min and max speeds, along
with different amounts of attraction to similar colos and repulsion from different colors. They
start out with random velocity, and they try to fly towards similarly-colored birds and away from
differently-colored birds. They leave trails as they go, there might be like swoopy rainbows and shit.

 - attraction/repulsion is based on absolute magnitude of difference in hue
 - should forces be inverse squared (prefer local) or squared (more eagerly try to minimize energy);
   how to make that easily switchable

## Themes
self-interaction, boyds, trails, emergent behavior

## Result
I couldn't get the triangles to render correctly; the lower points were always near the origin. So,
I made them balls. Overally, I'd say "not bad", but the attraction/repulsion parameters are very
sensitive. It's hard to see if the birbs will form stable dynamic patterns based on their color,
because if you have more than a few, they all just start to clump (so, I guess it works, but it
doesn't look great.)

![enh...](./1106.png)

## UPDATE!
I returned to this the next day, and got it working! Though there are still some angles where the
triangles render as lines, so idunno. Still, this is much closer to what I had in mind.

![woo!](./next_day_success.png)
